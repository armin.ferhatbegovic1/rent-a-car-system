// Generated code from Butter Knife. Do not modify!
package no.nordicsemi.android.blinky;

import android.view.View;
import android.widget.Button;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ScannerActivity_ViewBinding implements Unbinder {
  private ScannerActivity target;

  private View view7f080014;

  private View view7f08001b;

  private View view7f080013;

  private View view7f080012;

  @UiThread
  public ScannerActivity_ViewBinding(ScannerActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ScannerActivity_ViewBinding(final ScannerActivity target, View source) {
    this.target = target;

    View view;
    target.mScanningView = Utils.findRequiredView(source, R.id.state_scanning, "field 'mScanningView'");
    target.mEmptyView = Utils.findRequiredView(source, R.id.no_devices, "field 'mEmptyView'");
    target.mNoLocationPermissionView = Utils.findRequiredView(source, R.id.no_location_permission, "field 'mNoLocationPermissionView'");
    view = Utils.findRequiredView(source, R.id.action_grant_location_permission, "field 'mGrantPermissionButton' and method 'onGrantLocationPermissionClicked'");
    target.mGrantPermissionButton = Utils.castView(view, R.id.action_grant_location_permission, "field 'mGrantPermissionButton'", Button.class);
    view7f080014 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onGrantLocationPermissionClicked();
      }
    });
    view = Utils.findRequiredView(source, R.id.action_permission_settings, "field 'mPermissionSettingsButton' and method 'onPermissionSettingsClicked'");
    target.mPermissionSettingsButton = Utils.castView(view, R.id.action_permission_settings, "field 'mPermissionSettingsButton'", Button.class);
    view7f08001b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onPermissionSettingsClicked();
      }
    });
    target.mNoLocationView = Utils.findRequiredView(source, R.id.no_location, "field 'mNoLocationView'");
    target.mNoBluetoothView = Utils.findRequiredView(source, R.id.bluetooth_off, "field 'mNoBluetoothView'");
    view = Utils.findRequiredView(source, R.id.action_enable_location, "method 'onEnableLocationClicked'");
    view7f080013 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onEnableLocationClicked();
      }
    });
    view = Utils.findRequiredView(source, R.id.action_enable_bluetooth, "method 'onEnableBluetoothClicked'");
    view7f080012 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onEnableBluetoothClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ScannerActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mScanningView = null;
    target.mEmptyView = null;
    target.mNoLocationPermissionView = null;
    target.mGrantPermissionButton = null;
    target.mPermissionSettingsButton = null;
    target.mNoLocationView = null;
    target.mNoBluetoothView = null;

    view7f080014.setOnClickListener(null);
    view7f080014 = null;
    view7f08001b.setOnClickListener(null);
    view7f08001b = null;
    view7f080013.setOnClickListener(null);
    view7f080013 = null;
    view7f080012.setOnClickListener(null);
    view7f080012 = null;
  }
}
