// Generated code from Butter Knife. Do not modify!
package no.nordicsemi.android.blinky;

import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BlinkyActivity_ViewBinding implements Unbinder {
  private BlinkyActivity target;

  private View view7f08000e;

  @UiThread
  public BlinkyActivity_ViewBinding(BlinkyActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public BlinkyActivity_ViewBinding(final BlinkyActivity target, View source) {
    this.target = target;

    View view;
    target.mLed = Utils.findRequiredViewAsType(source, R.id.led_switch, "field 'mLed'", Switch.class);
    target.mButtonState = Utils.findRequiredViewAsType(source, R.id.button_state, "field 'mButtonState'", TextView.class);
    view = Utils.findRequiredView(source, R.id.action_clear_cache, "method 'onTryAgainClicked'");
    view7f08000e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onTryAgainClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    BlinkyActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mLed = null;
    target.mButtonState = null;

    view7f08000e.setOnClickListener(null);
    view7f08000e = null;
  }
}
