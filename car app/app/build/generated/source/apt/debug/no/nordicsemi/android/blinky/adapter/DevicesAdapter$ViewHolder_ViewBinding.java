// Generated code from Butter Knife. Do not modify!
package no.nordicsemi.android.blinky.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;
import no.nordicsemi.android.blinky.R;

public final class DevicesAdapter$ViewHolder_ViewBinding implements Unbinder {
  private DevicesAdapter.ViewHolder target;

  @UiThread
  public DevicesAdapter$ViewHolder_ViewBinding(DevicesAdapter.ViewHolder target, View source) {
    this.target = target;

    target.deviceAddress = Utils.findRequiredViewAsType(source, R.id.device_address, "field 'deviceAddress'", TextView.class);
    target.deviceName = Utils.findRequiredViewAsType(source, R.id.device_name, "field 'deviceName'", TextView.class);
    target.rssi = Utils.findRequiredViewAsType(source, R.id.rssi, "field 'rssi'", ImageView.class);
  }

  @Override
  public void unbind() {
    DevicesAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.deviceAddress = null;
    target.deviceName = null;
    target.rssi = null;
  }
}
