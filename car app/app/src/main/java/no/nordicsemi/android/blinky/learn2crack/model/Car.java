package no.nordicsemi.android.blinky.learn2crack.model;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by armin on 2/3/19.
 */

public class Car {
    private String car_id;
    private String name;
    private String model;

    public String getCar_id() {
        return car_id;
    }

    public String getName() {
        return name;
    }

    public String getModel() {
        return model;
    }

    public void setCar_id(String car_id) {
        this.car_id = car_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
