package no.nordicsemi.android.blinky.learn2crack;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;

import androidx.fragment.app.FragmentActivity;
import no.nordicsemi.android.blinky.R;
import rx.subscriptions.CompositeSubscription;

public class ShowReservationInfo extends FragmentActivity {
    private SharedPreferences mSharedPreferences;
    private CompositeSubscription mSubscriptions;
    private Button viewReservations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubscriptions = new CompositeSubscription();
        setContentView(R.layout.show_reservation_info);
        initViews();
    }

    private void initViews()
    {
        viewReservations = (Button) findViewById(R.id.submit2);
        viewReservations.setOnClickListener(view -> goToViewReservationsScreen());
    }

    private void goToViewReservationsScreen() {
        Intent intent = new Intent(this, ViewReservationsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }
}

