package no.nordicsemi.android.blinky;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.FragmentActivity;
import no.nordicsemi.android.blinky.adapter.CarsAdapter;
import no.nordicsemi.android.blinky.learn2crack.ProfileActivity;
import no.nordicsemi.android.blinky.learn2crack.model.Car;
import no.nordicsemi.android.blinky.learn2crack.model.Response;
import no.nordicsemi.android.blinky.learn2crack.network.NetworkUtil;
import no.nordicsemi.android.blinky.learn2crack.utils.Constants;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ChooseReservationActivity extends FragmentActivity {
    private SharedPreferences mSharedPreferences;
    private CompositeSubscription mSubscriptions;
    private ProgressBar mProgressbar;
    private String mToken;
    private ArrayList<Car> arrayOfCars = null;
    private CarsAdapter adapter;
    private ListView listCarsView;
    private String startDate;
    private String endDate;
    private String userID;
    private Intent intent;
    private Intent intent2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubscriptions = new CompositeSubscription();
        setContentView(R.layout.activity_choose_car);
        initSharedPreferences();
        initViews();
        intent = this.getIntent();
        startDate = intent.getStringExtra(Constants.START_DATE);
        endDate = intent.getStringExtra(Constants.END_DATE);
        intent2 = new Intent(this, ProfileActivity.class);
// Create the adapter to convert the array to views
        adapter = new CarsAdapter(this, arrayOfCars, intent, userID, mToken);
        listCarsView = (ListView) findViewById(R.id.cars_container);
        getAvailableCars();
    }

    private void initSharedPreferences() {

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToken = mSharedPreferences.getString(Constants.TOKEN, "Null");
        userID = mSharedPreferences.getString(Constants.USER_ID, "Null");
        userID.toString();
    }


    private void initViews() {
        mProgressbar = (ProgressBar) findViewById(R.id.progress);
    }

    private void getAvailableCars() {


        mSubscriptions.add(NetworkUtil.getRetrofit(mToken).getFreeCarsAtSpecficDate(startDate, endDate)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));
    }


    private void handleResponse(List<Car> cars) {
        mProgressbar.setVisibility(View.GONE);
// Construct the data source
        arrayOfCars = new ArrayList<Car>(cars);
// Create the adapter to convert the array to views

        adapter = new CarsAdapter(this, arrayOfCars, intent, userID, mToken);
        listCarsView.setAdapter(adapter);
        listCarsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String tes = "test";
                startActivity(intent2);
            }
        });

    }


    private void handleError(Throwable error) {
        mProgressbar.setVisibility(View.VISIBLE);

        if (error instanceof HttpException) {

            Gson gson = new GsonBuilder().create();

            try {

                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody, Response.class);
                showSnackBarMessage(response.getMessage());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            showSnackBarMessage("Network Error !");
        }
    }

    private void showSnackBarMessage(String message) {

        Snackbar.make(findViewById(R.id.activity_profile), message, Snackbar.LENGTH_SHORT).show();

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }
}

