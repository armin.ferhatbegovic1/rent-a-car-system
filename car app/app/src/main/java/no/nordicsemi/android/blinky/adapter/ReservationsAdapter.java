package no.nordicsemi.android.blinky.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;

import no.nordicsemi.android.blinky.BlinkyActivity;
import no.nordicsemi.android.blinky.R;
import no.nordicsemi.android.blinky.ScannerActivity;
import no.nordicsemi.android.blinky.learn2crack.ShowReservationInfo;
import no.nordicsemi.android.blinky.learn2crack.model.Car;
import no.nordicsemi.android.blinky.learn2crack.model.Reservation;
import no.nordicsemi.android.blinky.learn2crack.model.Response;
import no.nordicsemi.android.blinky.learn2crack.network.NetworkUtil;
import no.nordicsemi.android.blinky.learn2crack.utils.Constants;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by armin on 2/3/19.
 */

public class ReservationsAdapter extends ArrayAdapter<Reservation> {

    private String userID;
    private String mToken;
    private ProgressBar mProgressbar;
    private CompositeSubscription mSubscriptions;
    private Context context;
    private SharedPreferences mSharedPreferences;

    public ReservationsAdapter(Context context, ArrayList<Reservation> reservations, Intent intent, String userID, String mToken) {
        super(context, 0, reservations);
        this.userID = userID;
        this.mToken = mToken;
        this.context = context;
        mSubscriptions = new CompositeSubscription();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Reservation reservation = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.reservation_item, parent, false);
        }
        TextView tvName = (TextView) convertView.findViewById(R.id.car_name);
        tvName.setText(reservation.getCar().getName()+"("+reservation.getCar().getModel()+")");
        TextView tvModel = (TextView) convertView.findViewById(R.id.reservation_date);
        tvModel.setText("Date: " + reservation.getStart_date()+" / "+reservation.getEnd_date()+"\n"+"Time: "+reservation.getStart_time()+" / "+reservation.getEnd_time());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(parent.getContext(), "You choose: " + reservation.getCar().getName(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, ScannerActivity.class);
                intent.putExtra(Constants.RESERVATION_TOKEN, reservation.getGeneratedToken());
                context.startActivity(intent);
            }
        });
        // Return the completed view to render on screen
        return convertView;
    }


    private void showSnackBarMessage(String message) {
        //  Snackbar.make(findViewById(R.id.activity_profile), message, Snackbar.LENGTH_SHORT).show();
    }

}
