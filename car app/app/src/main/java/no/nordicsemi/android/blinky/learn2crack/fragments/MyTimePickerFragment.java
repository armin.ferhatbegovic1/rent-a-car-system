package no.nordicsemi.android.blinky.learn2crack.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.fragment.app.DialogFragment;
import no.nordicsemi.android.blinky.R;

/**
 * Created by armin on 2/2/19.
 */

public class MyTimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{
    private Calendar c = null;
    private EditText DateEdit;
    public MyTimePickerFragment(EditText dateEdit) {
        DateEdit = dateEdit;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        //Use the current time as the default values for the time picker
         c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        //Create and return a new instance of TimePickerDialog
        return new TimePickerDialog(getActivity(), R.style.TimePickerTheme,this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    //onTimeSet() callback method
    public void onTimeSet(TimePicker view, int hourOfDay, int minute){
        String timeString=  String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date time = null;
        try {
            time = sdf.parse(timeString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        sdf = new SimpleDateFormat("hh:mm a");
        String formatedTime = sdf.format(time);
        DateEdit.setText(formatedTime);
        Toast.makeText(getActivity(), "selected time is " +time, Toast.LENGTH_SHORT).show();
    }
}