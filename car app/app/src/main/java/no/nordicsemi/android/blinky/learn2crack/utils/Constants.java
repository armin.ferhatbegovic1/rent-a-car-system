package no.nordicsemi.android.blinky.learn2crack.utils;

public class Constants {

    public static final String BASE_URL = "http:/192.168.100.4:4000/api/";
    public static  String TOKEN = "token";
    public static  String EMAIL = "email";
    public static String USER_ID = "user_id";
    private static String GENERATED_RESERVATION_TOKEN="generated_reservation_token";
    public static String RESERVATION="reservation";
    public static String START_DATE="START_DATE";
    public static String END_DATE="END_DATE";

    public static String START_TIME="START_TIME";
    public static String END_TIME="END_TIME";
    public static String RESERVATION_TOKEN="RESERVATION_TOKEN";

}
