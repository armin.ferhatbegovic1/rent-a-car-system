package no.nordicsemi.android.blinky.learn2crack.network;

import java.util.List;

import no.nordicsemi.android.blinky.learn2crack.model.Car;
import no.nordicsemi.android.blinky.learn2crack.model.Cars;
import no.nordicsemi.android.blinky.learn2crack.model.Reservation;
import no.nordicsemi.android.blinky.learn2crack.model.Response;
import no.nordicsemi.android.blinky.learn2crack.model.User;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;

public interface RetrofitInterface {

    @POST("users")
    Observable<Response> register(@Body User user);

    @POST("user/login-customer")
    Observable<Response> login();

    @GET("user/profile-by-email/{email}")
    Observable<User> getProfile(@Path("email") String email);

    @GET("reservation/get-free-cars/{start_date}/{end_date}")
    Observable<List<Car>> getFreeCarsAtSpecficDate(@Path("start_date") String start_date, @Path("end_date") String end_date);

    @GET("reservation/list-reservations-by-user/{user_id}")
    Observable<List<Reservation>> getReservations(@Path("user_id") String user_id);


    @POST("reservation/make-reservation/{start_date}/{end_date}/{start_time}/{end_time}/{car_id}/{user_id}")
    Observable<String> getReservationsToken(@Path("start_date") String start_date, @Path("end_date") String end_date,@Path("start_time") String start_time, @Path("end_time") String end_time,@Path("car_id") String car_id, @Path("user_id") String user_id);


    @PUT("users/{email}")
    Observable<Response> changePassword(@Path("email") String email, @Body User user);

    @POST("users/{email}/password")
    Observable<Response> resetPasswordInit(@Path("email") String email);

    @POST("users/{email}/password")
    Observable<Response> resetPasswordFinish(@Path("email") String email, @Body User user);
}
