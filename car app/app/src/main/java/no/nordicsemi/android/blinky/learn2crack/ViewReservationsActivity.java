package no.nordicsemi.android.blinky.learn2crack;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.FragmentActivity;
import no.nordicsemi.android.blinky.R;
import no.nordicsemi.android.blinky.adapter.CarsAdapter;
import no.nordicsemi.android.blinky.adapter.ReservationsAdapter;
import no.nordicsemi.android.blinky.learn2crack.model.Car;
import no.nordicsemi.android.blinky.learn2crack.model.Reservation;
import no.nordicsemi.android.blinky.learn2crack.model.Response;
import no.nordicsemi.android.blinky.learn2crack.network.NetworkUtil;
import no.nordicsemi.android.blinky.learn2crack.utils.Constants;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ViewReservationsActivity extends FragmentActivity {
    private SharedPreferences mSharedPreferences;
    private CompositeSubscription mSubscriptions;
    private ProgressBar mProgressbar;
    private String mToken;
    private ArrayList<Reservation> arrayOfReservations = null;
    private ReservationsAdapter adapter;
    private ListView listReservationsView;
    private String userID;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubscriptions = new CompositeSubscription();
        setContentView(R.layout.activity_choose_reservation);
        initSharedPreferences();
        initViews();
        intent = this.getIntent();
        adapter = new ReservationsAdapter(this, arrayOfReservations, intent, userID, mToken);
        listReservationsView = (ListView) findViewById(R.id.cars_container);
        getReservations();
    }

    private void initSharedPreferences() {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToken = mSharedPreferences.getString(Constants.TOKEN, "Null");
        userID = mSharedPreferences.getString(Constants.USER_ID, "Null");
        userID.toString();
    }


    private void initViews() {
        mProgressbar = (ProgressBar) findViewById(R.id.progress);
    }

    private void getReservations() {
        mSubscriptions.add(NetworkUtil.getRetrofit(mToken).getReservations(userID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));
    }


    private void handleResponse(List<Reservation> reservations) {
        mProgressbar.setVisibility(View.GONE);
// Construct the data source
        arrayOfReservations = new ArrayList<Reservation>(reservations);
// Create the adapter to convert the array to views

        adapter = new ReservationsAdapter(this, arrayOfReservations, intent, userID, mToken);
        listReservationsView.setAdapter(adapter);


    }


    private void handleError(Throwable error) {
        mProgressbar.setVisibility(View.VISIBLE);

        if (error instanceof HttpException) {

            Gson gson = new GsonBuilder().create();

            try {

                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody, Response.class);
                showSnackBarMessage(response.getMessage());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            showSnackBarMessage("Network Error !");
        }
    }

    private void showSnackBarMessage(String message) {

        Snackbar.make(findViewById(R.id.activity_profile), message, Snackbar.LENGTH_SHORT).show();

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }
}

