package no.nordicsemi.android.blinky.learn2crack.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import androidx.fragment.app.DialogFragment;
import no.nordicsemi.android.blinky.R;

/**
 * Created by armin on 2/2/19.
 */

public class MyDatePickerFragment extends DialogFragment {

    private Calendar c = null;
    private EditText DateEdit;

    public MyDatePickerFragment(EditText dateEdit) {
        DateEdit = dateEdit;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), R.style.TimePickerTheme, dateSetListener, year, month, day);
    }

    private DatePickerDialog.OnDateSetListener dateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    String date= view.getYear() + "-" + String.format("%02d", (view.getMonth() + 1)) + "-" + String.format("%02d",view.getDayOfMonth());

                    DateEdit.setText(date);
                    Toast.makeText(getActivity(), "selected date is " + view.getYear() +
                            " / " + (view.getMonth() + 1) +
                            " / " + view.getDayOfMonth(), Toast.LENGTH_SHORT).show();
                }
            };

    public Calendar getCalendar() {
        return c;
    }
}