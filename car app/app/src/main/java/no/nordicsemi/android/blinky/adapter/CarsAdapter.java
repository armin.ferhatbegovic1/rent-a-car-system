package no.nordicsemi.android.blinky.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import no.nordicsemi.android.blinky.R;
import no.nordicsemi.android.blinky.ScannerActivity;
import no.nordicsemi.android.blinky.learn2crack.ProfileActivity;
import no.nordicsemi.android.blinky.learn2crack.ShowReservationInfo;
import no.nordicsemi.android.blinky.learn2crack.model.Car;
import no.nordicsemi.android.blinky.learn2crack.model.Response;
import no.nordicsemi.android.blinky.learn2crack.network.NetworkUtil;
import no.nordicsemi.android.blinky.learn2crack.utils.Constants;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static androidx.core.content.ContextCompat.startActivity;

/**
 * Created by armin on 2/3/19.
 */

public class CarsAdapter extends ArrayAdapter<Car> {
    private String startDate;
    private String endDate;
    private String startTime;
    private String endTime;
    private int carID;
    private String userID;
    private String mToken;
    private ProgressBar mProgressbar;
    private CompositeSubscription mSubscriptions;
    private Context context;
    private SharedPreferences mSharedPreferences;

    public CarsAdapter(Context context, ArrayList<Car> cars, Intent intent, String userID, String mToken) {
        super(context, 0, cars);
        startDate = intent.getStringExtra(Constants.START_DATE);
        endDate = intent.getStringExtra(Constants.END_DATE);
        startTime = intent.getStringExtra(Constants.START_TIME);
        endTime = intent.getStringExtra(Constants.END_TIME);
        this.userID = userID;
        this.mToken = mToken;
        this.context = context;
        mSubscriptions = new CompositeSubscription();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Car car = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.car_item, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.car_name);

        // Populate the data into the template view using the data object
        tvName.setText(car.getName());
        // Lookup view for data population
        TextView tvModel = (TextView) convertView.findViewById(R.id.car_model);

        // Populate the data into the template view using the data object
        tvModel.setText("Model: " + car.getModel());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(parent.getContext(), "You choose: " + car.getName(), Toast.LENGTH_SHORT).show();
                getReservationsToken(car.getCar_id());
                Intent intent = new Intent(context, ShowReservationInfo.class);
                context.startActivity(intent);
            }
        });
        // Return the completed view to render on screen
        return convertView;
    }

    private void getReservationsToken(String carID) {
        mSubscriptions.add(NetworkUtil.getRetrofit(mToken).getReservationsToken(startDate, endDate, startTime, endTime, carID, userID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));

    }

    private void handleResponse(String reservationToken) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constants.RESERVATION_TOKEN, reservationToken);
        editor.apply();
    }


    private void handleError(Throwable error) {
        // mProgressbar.setVisibility(View.VISIBLE);

        if (error instanceof HttpException) {

            Gson gson = new GsonBuilder().create();

            try {

                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody, Response.class);
                showSnackBarMessage(response.getMessage());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            showSnackBarMessage("Network Error !");
        }
    }

    private void showSnackBarMessage(String message) {

        //  Snackbar.make(findViewById(R.id.activity_profile), message, Snackbar.LENGTH_SHORT).show();

    }

}
