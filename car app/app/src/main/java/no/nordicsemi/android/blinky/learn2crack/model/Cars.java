package no.nordicsemi.android.blinky.learn2crack.model;

import java.util.List;

/**
 * Created by armin on 2/3/19.
 */

public class Cars {
    private List<Car> cars;

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }
}
