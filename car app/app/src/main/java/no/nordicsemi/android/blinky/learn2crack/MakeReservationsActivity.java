package no.nordicsemi.android.blinky.learn2crack;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import no.nordicsemi.android.blinky.R;
import no.nordicsemi.android.blinky.learn2crack.fragments.MyDatePickerFragment;
import no.nordicsemi.android.blinky.learn2crack.fragments.MyTimePickerFragment;
import no.nordicsemi.android.blinky.learn2crack.utils.Constants;

public class MakeReservationsActivity extends FragmentActivity {
    static EditText DateEdit;
    static EditText DateEdit2;
    static EditText TimeEdit1;
    static EditText TimeEdit2;
    private Button Submit;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_reservations);

        initViews();


    }

    private void initViews() {
        DateEdit = (EditText) findViewById(R.id.editText1);
        DateEdit2 = (EditText) findViewById(R.id.editText2);
        TimeEdit1 = (EditText) findViewById(R.id.editText3);
        TimeEdit2 = (EditText) findViewById(R.id.editText4);
        Submit = (Button) findViewById(R.id.submit);
        Submit.setOnClickListener(view -> goToChooseCarScreen());
    }


    private void goToChooseCarScreen() {
        Intent intent = new Intent(this, ChooseCarActivity.class);


        intent.putExtra (Constants.START_DATE,DateEdit.getText().toString());
        intent.putExtra (Constants.END_DATE,DateEdit2.getText().toString());

        intent.putExtra (Constants.START_TIME,TimeEdit1.getText().toString());
        intent.putExtra (Constants.END_TIME,TimeEdit2.getText().toString());

        startActivity(intent);
    }

    public void showDatePicker(View v) {
        DialogFragment newFragment = new MyDatePickerFragment(DateEdit);
        newFragment.show(getSupportFragmentManager(), "date picker");
    }

    public void showDatePicker2(View v) {
        DialogFragment newFragment2 = new MyDatePickerFragment(DateEdit2);
        newFragment2.show(getSupportFragmentManager(), "date picker");
    }

    public void showTimePicker(View v) {
        DialogFragment newFragment3 = new MyTimePickerFragment(TimeEdit1);
        newFragment3.show(getSupportFragmentManager(), "Time picker");
    }

    public void showTimePicker2(View v) {
        DialogFragment newFragment4 = new MyTimePickerFragment(TimeEdit2);
        newFragment4.show(getSupportFragmentManager(), "Time picker2");
    }
}

