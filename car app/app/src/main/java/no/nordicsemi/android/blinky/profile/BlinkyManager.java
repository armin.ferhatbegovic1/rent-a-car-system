/*
 * Copyright (c) 2018, Nordic Semiconductor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package no.nordicsemi.android.blinky.profile;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import no.nordicsemi.android.ble.BleManager;
import no.nordicsemi.android.ble.ReadRequest;
import no.nordicsemi.android.ble.data.Data;
import no.nordicsemi.android.blinky.profile.callback.BlinkyButtonDataCallback;
import no.nordicsemi.android.blinky.profile.callback.BlinkyLedDataCallback;
import no.nordicsemi.android.blinky.profile.data.BlinkyLED;
import no.nordicsemi.android.log.LogContract;
import no.nordicsemi.android.log.LogSession;
import no.nordicsemi.android.log.Logger;

public class BlinkyManager extends BleManager<BlinkyManagerCallbacks> {
    /**
     * Nordic Blinky Service UUID.
     */
    public final static UUID AUTHORIZATION_UUID_SERVICE = UUID.fromString("00002009-0000-1000-8000-00805f9b34fb");
    /**
     * BUTTON characteristic UUID.
     */
    private final static UUID AUTHORIZATION_KEY_1_UUID_CHAR = UUID.fromString("000020ca-0000-1000-8000-00805f9b34fb");
    private final static UUID AUTHORIZATION_KEY_2_UUID_CHAR = UUID.fromString("000020cd-0000-1000-8000-00805f9b34fb");
    private final static UUID AUTHORIZATION_HASH_UUID_CHAR = UUID.fromString("000020ce-0000-1000-8000-00805f9b34fb");
    List<BluetoothGattCharacteristic> chars = new ArrayList<>();

    /**
     * LED characteristic UUID.
     */
    private final static UUID LBS_UUID_LED_CHAR = UUID.fromString("00001525-1212-efde-1523-785feabcd123");

    private BluetoothGattCharacteristic mButtonCharacteristic, mLedCharacteristic, mAuth1Characteristic, mAuth2Characteristic, mHashCharacteristic;
    private LogSession mLogSession;
    private boolean mSupported;
    private boolean mLedOn;

    public BlinkyManager(@NonNull final Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected BleManagerGattCallback getGattCallback() {
        return mGattCallback;
    }

    /**
     * Sets the log session to be used for low level logging.
     *
     * @param session the session, or null, if nRF Logger is not installed.
     */
    public void setLogger(@Nullable final LogSession session) {
        this.mLogSession = session;
    }

    @Override
    public void log(final int priority, @NonNull final String message) {
        // The priority is a Log.X constant, while the Logger accepts it's log levels.
        Logger.log(mLogSession, LogContract.Log.Level.fromPriority(priority), message);
    }

    @Override
    protected boolean shouldClearCacheWhenDisconnected() {
        return !mSupported;
    }

    /**
     * The Button callback will be notified when a notification from Button characteristic
     * has been received, or its data was read.
     * <p>
     * If the data received are valid (single byte equal to 0x00 or 0x01), the
     * {@link BlinkyButtonDataCallback#onButtonStateChanged} will be called.
     * Otherwise, the {@link BlinkyButtonDataCallback#onInvalidDataReceived(BluetoothDevice, Data)}
     * will be called with the data received.
     */
    private final BlinkyButtonDataCallback mButtonCallback = new BlinkyButtonDataCallback() {
        @Override
        public void onButtonStateChanged(@NonNull final BluetoothDevice device,
                                         final boolean pressed) {
            log(LogContract.Log.Level.APPLICATION, "Button " + (pressed ? "pressed" : "released"));
            mCallbacks.onButtonStateChanged(device, pressed);
        }

        @Override
        public void onInvalidDataReceived(@NonNull final BluetoothDevice device,
                                          @NonNull final Data data) {
            log(Log.WARN, "Invalid data received: " + data);
        }
    };

    /**
     * The LED callback will be notified when the LED state was read or sent to the target device.
     * <p>
     * This callback implements both {@link no.nordicsemi.android.ble.callback.DataReceivedCallback}
     * and {@link no.nordicsemi.android.ble.callback.DataSentCallback} and calls the same
     * method on success.
     * <p>
     * If the data received were invalid, the
     * {@link BlinkyLedDataCallback#onInvalidDataReceived(BluetoothDevice, Data)} will be
     * called.
     */
    private final BlinkyLedDataCallback mLedCallback = new BlinkyLedDataCallback() {
        @Override
        public void onLedStateChanged(@NonNull final BluetoothDevice device,
                                      final boolean on) {
            mLedOn = on;
            log(LogContract.Log.Level.APPLICATION, "LED " + (on ? "ON" : "OFF"));
            mCallbacks.onLedStateChanged(device, on);
        }

        @Override
        public void onInvalidDataReceived(@NonNull final BluetoothDevice device,
                                          @NonNull final Data data) {
            // Data can only invalid if we read them. We assume the app always sends correct data.
            log(Log.WARN, "Invalid data received: " + data);
        }
        @Override
        public void onGeneratingHash(@NonNull final BluetoothDevice device, final byte[] hash){
            writeCharacteristic(mHashCharacteristic,hash);
        }
    };

    private final BlinkyLedDataCallback mLedCallback2 = new BlinkyLedDataCallback() {
        @Override
        public void onLedStateChanged(@NonNull final BluetoothDevice device,
                                      final boolean on) {
            mLedOn = on;
            log(LogContract.Log.Level.APPLICATION, "LED " + (on ? "ON" : "OFF"));
            mCallbacks.onLedStateChanged(device, on);
        }

        @Override
        public void onInvalidDataReceived(@NonNull final BluetoothDevice device,
                                          @NonNull final Data data) {
            // Data can only invalid if we read them. We assume the app always sends correct data.
            log(Log.WARN, "Invalid data received: " + data);
        }
        @Override
        public void onGeneratingHash(@NonNull final BluetoothDevice device, final byte[] hash){
            writeCharacteristic(mHashCharacteristic,hash)
                    .with(mLedCallback).enqueue();
        }
    };

    /**
     * BluetoothGatt callbacks object.
     */
    private final BleManagerGattCallback mGattCallback = new BleManagerGattCallback() {
        @Override
        protected void initialize() {
            setNotificationCallback(mButtonCharacteristic).with(mButtonCallback);
            readCharacteristic(mAuth1Characteristic).with(mLedCallback).enqueue();
            readCharacteristic(mAuth2Characteristic).with(mLedCallback2).enqueue();
            enableNotifications(mButtonCharacteristic).enqueue();


        }

        @Override
        public boolean isRequiredServiceSupported(@NonNull final BluetoothGatt gatt) {
            final BluetoothGattService service = gatt.getService(AUTHORIZATION_UUID_SERVICE);


            if (service != null) {
                mAuth1Characteristic = service.getCharacteristic(AUTHORIZATION_KEY_1_UUID_CHAR);
                mAuth2Characteristic = service.getCharacteristic(AUTHORIZATION_KEY_2_UUID_CHAR);


                mHashCharacteristic = service.getCharacteristic(AUTHORIZATION_HASH_UUID_CHAR);

             /*   gatt.readCharacteristic(mAuth1Characteristic);
                gatt.readCharacteristic(mAuth2Characteristic);

                gatt.setCharacteristicNotification(mAuth1Characteristic, true);
                gatt.setCharacteristicNotification(mAuth2Characteristic, true);*/



             //   generateHashKey(gatt);


          /*     mAuth1Characteristic.getDescriptor(AUTHORIZATION_KEY_2_UUID_CHAR);
                mAuth2Characteristic.getDescriptor(AUTHORIZATION_KEY_1_UUID_CHAR);

                gatt.writeCharacteristic(mAuth1Characteristic);
                gatt.writeCharacteristic(mAuth2Characteristic);*/

            }


            boolean writeRequest = true;
            if (mHashCharacteristic != null) {
                final int rxProperties = mHashCharacteristic.getProperties();
                writeRequest = (rxProperties & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0;
            }
         //   generateHashKey();
            mSupported = mAuth1Characteristic != null && mAuth2Characteristic != null && writeRequest;
            return mSupported;
        }


        @Override
        protected void onDeviceDisconnected() {
            mAuth1Characteristic = null;
            mAuth2Characteristic = null;
            mHashCharacteristic = null;
        }
    };

    /**
     * Sends a request to the device to turn the LED on or off.
     *
     * @param on true to turn the LED on, false to turn it off.
     */
    public void send(final boolean on) {
        // Are we connected?
        if (mHashCharacteristic == null)
            return;

        // No need to change?
        if (mLedOn == on)
            return;

        log(Log.VERBOSE, "Turning LED " + (on ? "ON" : "OFF") + "...");
        writeCharacteristic(mLedCharacteristic, on ? BlinkyLED.turnOn() : BlinkyLED.turnOff())
                .with(mLedCallback).enqueue();
    }

    /*public void sendHashKey(final boolean on) {
        // Are we connected?
        if (mHashCharacteristic == null)
            return;

        // No need to change?
        if (mLedOn == on)
            return;

        writeCharacteristic(mHashCharacteristic, on ? BlinkyLED.turnOn() : BlinkyLED.turnOff())
                .with(mLedCallback).enqueue();
    }
*/
}
