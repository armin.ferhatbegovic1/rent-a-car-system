New platform was designed for renting, accessing and tracking vehicles.
The implementation and integration of server, android, web and embeded application wasperformed on nRF52 Soc using BLE and REST interface (WEB service) together with SierraWireless modem (4G).The design of the platform seeks to explain the possibilities and limitations of networkhardware  and  show  how  to  use  communication  services,  and  understand  advantages  anddisadvantages of selected technologies.The primary idea of car sharing using this platform is the ability of user to rent a vehiclewith a mobile application, where the location of all available vehicles is determined by GPScoordinates, and the process of accessing the car is provided through BLE technology.With the support of GPS technology, the microcontroller will broadcast the position andstatus of the vehicle, which allows the administrator/service provider safe, reliable and effi-cient monitoring.

Sub systems:
1. React native/Android app for customer
2. Nodejs backend app for tracking, payment , registration functionality
3. Web app (ReactJS) for administration
4. Embeded C app for authorization authentication user to vehicle.
