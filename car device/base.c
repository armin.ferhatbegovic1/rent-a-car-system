#include "base.h"
volatile uint32_t g_base_sensor_update_time = 0;
volatile uint32_t g_base_fast_sensor_update_time = 0;

void initBASE(void)
{
	uint8_t k;
	uint32_t utmp32;
	
	initUART0(19, 20, UART0_BAUDRATE_921600);
	printUART0("\nwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww\n",0);
	printUART0("w Starting BLE nRF52832 device...",0);
	printUART0("\nwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww\n",0);
	
	initBLE();
	
	startCLOCK(CLOCK_LF);
	initRTC2();
	
	if ((NRF_UICR->NFCPINS & UICR_NFCPINS_PROTECT_Msk) == (UICR_NFCPINS_PROTECT_NFC << UICR_NFCPINS_PROTECT_Pos))
	{// if the NFC pins P0.10 & P0.09 are used as GPIO's we need to disable NFC function which is enabled
	 // by default
		NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos;
		while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}            
		NRF_UICR->NFCPINS &= ~UICR_NFCPINS_PROTECT_Msk;
		while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}            
		NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos;
		while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}            
		NVIC_SystemReset();
	}
	
	g_base_sensor_update_time = getRTC2();


}

volatile uint32_t g_base_cnt = 0;
