#include "nrf52.h"
#include "base.h"
#include "ble-base.h"
#include "battery.h"
#include "config.h"
#include "authorization.h"
// RC BASED LF !!!!
// in ./softdevice/s132/headers/ble_gatts.h:
// change 
// #define BLE_GATTS_ATTR_TAB_SIZE_DEFAULT     0x0000
// to
// #define BLE_GATTS_ATTR_TAB_SIZE_DEFAULT     0x0980
// in nrf52832 linker script increase the RAM size used by softdevice by 0x0400


int main(void)
{
	

	
	initLEDs();
	initBASE();															// initialize base system 
    uint32_t time = getRTC2();
    g_bat_lvl[0] = 0;
    initAuthorizationsKeys();
    uint8_t k;

	while(1)
    {	
		chk4TxBLESEN();													// check if we have to stream the data
		if(chk4TimeoutRTC2(time, 1000) == (RTC_TIMEOUT))
		{
			g_bat_lvl[0] = g_bat_lvl[0] + 1;
			time = getRTC2();

    
    chkBLESCF();
    }}
    
    return 0;
}

