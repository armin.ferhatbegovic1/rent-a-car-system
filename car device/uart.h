#ifndef __UART_H_
#define __UART_H_

#include <stdint.h>
#include "nrf52.h"
#include "misc.h"

#define UART0_BAUDRATE_9600					0x00275000	
#define UART0_BAUDRATE_115200				0x01D7E000	
#define UART0_BAUDRATE_230400				0x03AFB000	
#define UART0_BAUDRATE_460800				0x075F7000	
#define UART0_BAUDRATE_921600				0x0EBEDFA4			

#define UART_RX_BUFFER_SIZE					1024	

void initUART0(uint8_t tx, uint8_t rx, uint32_t baudrate);
void deinitUART0(void);
void clrBuffUART0(void);
void putcharUART0(uint8_t data);
void printUART0(char * str, ... );
void sprintUART0(uint8_t * str);


extern volatile uint8_t g_uart_buff[UART_RX_BUFFER_SIZE];
extern volatile uint16_t g_uart_widx; 
extern volatile uint16_t g_uart_ridx;
#endif 
