#ifndef __BATTERY_H_
#define __BATTERY_H_

#include "nrf52.h"
#include "clock.h"
#include "config.h"
#include "uart.h"

#define BATTERY_NOMINAL_VOLTAGE_MV				3700
#define CHARGING_PIN							3

#define BATTERY_EXTERNAL_POWER_CONNECTED		0x01
#define BATTERY_EXTERNAL_POWER_DISCONNECTED		0x00

uint8_t 	getLvlBATTERY(void);
uint8_t 	chk4ChargingBATTERY(void);

extern volatile uint8_t g_bat_charging_state[1];
#endif 
