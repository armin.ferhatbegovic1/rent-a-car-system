#ifndef __CONFIG_H_
#define __CONFIG_H_

#include "uart.h"

#define NRF52_SYSTEM_DEBUG	


#define BLE_CHAR_BAT_LVL_ARRAY_SIZE					10
#define BLE_CHAR_RTC_DATA_ARRAY_SIZE 				4
#define BLE_CHAR_BAT_CHARGE_STATE_ARRAY_SIZE		1
#define BLE_CHAR_ACCEL_ARRAY_SIZE					6
#define BLE_CHAR_ECG_ARRAY_SIZE						9
#define BLE_CHAR_PACE_ARRAY_SIZE					6
#define BLE_CHAR_EXTTEMP_ARRAY_SIZE					2
#define BLE_CHAR_SUM_ARRAY_SIZE						20
#define LED_1		17
#define LED_2		18
#define LED_3		19
#define LED_4		20
///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
/// Pripherals assignments
///---------------------------------------------------------------------
/// RTC0
/// RTC1
/// RTC2 	- active/sleep mode 1sec base clock 
///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
/// Pinout assignment
///---------------------------------------------------------------------

/// P0.18   - UART0 Tx debug
/// P0.19   - UART0 Rx debug
///
///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww


extern volatile uint8_t g_bat_lvl[BLE_CHAR_SUM_ARRAY_SIZE];
void initLEDs();
void turnOnLED();
void turnOffLED();
#endif 
