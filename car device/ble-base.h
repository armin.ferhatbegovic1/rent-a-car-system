#ifndef __BLE_BASE_H_
#define __BLE_BASE_H_

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf52.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_conn_params.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "ble_gap.h"
//#include "led.h"

#include "uart.h"
#include "ble-sensor.h"


#define BLE_INDICATION_RECEIVED			0
#define BLE_INDICATION_NOT_RECEIVED		1

void 	initBLE(void);
void 	chkBLE(void);

#endif
