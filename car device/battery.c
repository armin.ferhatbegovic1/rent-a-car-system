#include "battery.h"

volatile uint8_t g_bat_charging_state[1] = {0};

uint8_t getLvlBATTERY(void)
{/// checks battery level and returns value [0-100]
	uint16_t k;
	int16_t adc_data;
	//wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	// Capture battery voltage prescaled with 1/2 
	//------------------------------------------------------------------
	{
		NRF_SAADC->RESOLUTION = 3;										// 14 bit resolution for AIN0
		NRF_SAADC->CH[1].PSELP = 0x00000001;							// select P0.2 pin aka AIN0 as ADC input
		NRF_SAADC->CH[1].PSELN = 0x00000000;							// NC
		NRF_SAADC->CH[1].CONFIG = 0x00050000;							// single ended, bypass positive & negative lader
																		// gain = 1/6, internal reference 0.6V, 40us sample time
		
		NRF_SAADC->OVERSAMPLE = 0;										// bypass oversample
		NRF_SAADC->SAMPLERATE = 0;										// sample rate controlled by SAMPLE task							
		NRF_SAADC->RESULT.PTR = (uint32_t)(&adc_data);
		//NRF_SAADC->RESULT.AMOUNT = 1;
		NRF_SAADC->RESULT.MAXCNT = 1;
		
		NRF_SAADC->ENABLE = 1;											// enable SAADC
		NRF_SAADC->TASKS_START = 1;
		NRF_SAADC->TASKS_SAMPLE = 1;	
		
		for(k=0;k<100;k++)													// wait for conversion to end
		{
			delay_us(100);
			if(NRF_SAADC->EVENTS_END)
			{
				break;
			}
		}
			
		NRF_SAADC->EVENTS_END	= 0;
		NRF_SAADC->ENABLE = 0;											// disable SAADC
	}


	int32_t voltage  = (adc_data*72*1000)/(16*1024*10);					// battery voltage in mV
//#ifdef NRF52_SYSTEM_DEBUG	
	//printUART0("-> BAT: Voltage [%d]mV\n",&voltage);
//#endif
	int16_t lvl = (voltage*100)/(BATTERY_NOMINAL_VOLTAGE_MV);
	
	if(lvl > 100)
		lvl = 100;
		
	if(lvl < 0)
		lvl = 0;
	
	g_bat_lvl[0] = lvl;
		
//#ifdef NRF52_SYSTEM_DEBUG	
	//voltage = lvl;
	//printUART0("-> BAT: LVL [%d]\n",&voltage);
//#endif
		
	return lvl;
}

uint8_t chk4ChargingBATTERY(void)
{/// checks if external power supply is connected for battery charging
	uint8_t r_val = (BATTERY_EXTERNAL_POWER_DISCONNECTED);
	
	// configure external power supply detection pin as input
	NRF_P0->PIN_CNF[CHARGING_PIN] = 0x00000000;
	if(NRF_P0->IN &(1<<(CHARGING_PIN)))
		r_val = (BATTERY_EXTERNAL_POWER_CONNECTED);
	
	
	// disconnect external power supply detection pin
	NRF_P0->PIN_CNF[CHARGING_PIN] = 0x00000002;
	
	g_bat_charging_state[0] = r_val;
	
	return r_val;
}



