#ifndef __SHA1_H_
#define __SHA1_H_

#include "config.h"

#define HASH_VALIDATION_OK				0x00
#define HASH_VALIDATION_ERROR			0x01
#define BLE_CHAR_SIZE_HASH				20
void 	 genSHA(uint8_t * vec1, uint8_t * vec2, uint8_t * g_sha_gen_hash);
void	 getRNG(uint8_t * result, uint8_t size);
 uint8_t chkHASH(uint8_t*g_sha_rx_hash,uint8_t*g_sha_gen_hash);

extern volatile uint32_t tA;
extern volatile uint32_t tB;
extern volatile uint32_t tC;
extern volatile uint32_t tD;
extern volatile uint32_t tE;

#endif 
