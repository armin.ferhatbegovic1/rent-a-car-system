#include "ble-base.h"
#include "authorization.h"

#define CENTRAL_LINK_COUNT              0                                           /**<number of central links used by the application. When changing this number remember to adjust the RAM settings*/
#define PERIPHERAL_LINK_COUNT           1                                           /**<number of peripheral links used by the application. When changing this number remember to adjust the RAM settings*/

#define DEVICE_NAME                     "BMW 318i"                             	/**< Name of device. Will be included in the advertising data. */

#define APP_ADV_INTERVAL                64                                          /**< The advertising interval (in units of 0.625 ms; this value corresponds to 40 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      BLE_GAP_ADV_TIMEOUT_GENERAL_UNLIMITED       /**< The advertising time-out (in units of seconds). When set to 0, we will never time out. */

#define APP_TIMER_PRESCALER             0                                           /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_MAX_TIMERS            1                                           /**< Maximum number of simultaneously created timers. */
#define APP_TIMER_OP_QUEUE_SIZE         2                                           /**< Size of timer operation queues. */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(24, UNIT_1_25_MS)            /**< Minimum acceptable connection interval (0.5 seconds). */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(48, UNIT_1_25_MS)            /**< Maximum acceptable connection interval (1 second). */

#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory time-out (4 seconds). */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(20000, APP_TIMER_PRESCALER) /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (15 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER)  /**< Time between each call to sd_ble_gap_conn_param_update after the first call (5 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

static uint16_t                         m_conn_handle = BLE_CONN_HANDLE_INVALID;    /**< Handle of the current connection. */
                                     

void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{/// assert macro callback
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

static void timers_init(void)
{/// init timers
    // Initialize timer module, making it use the scheduler
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);
}

static void gap_params_init(void)
{/// init generic access profile: name, appearance, and the preferred connection parameters
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}

static void advertising_init(void)
{/// init advertising
    uint32_t      err_code;
    ble_advdata_t advdata;
    ble_advdata_t scanrsp;

    ble_uuid_t adv_uuids[] = {{BLE_SYSTEM_SERVICE_UUID, BLE_UUID_TYPE_BLE},
							  {BLE_AUTHORIZATION_SERVICE_UUID, BLE_UUID_TYPE_BLE}
							  };

    // Build and set advertising data
    memset(&advdata, 0, sizeof(advdata));

    advdata.name_type          = BLE_ADVDATA_FULL_NAME;
    advdata.include_appearance = true;
    advdata.flags              = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;


    memset(&scanrsp, 0, sizeof(scanrsp));
    scanrsp.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    scanrsp.uuids_complete.p_uuids  = adv_uuids;

    err_code = ble_advdata_set(&advdata, &scanrsp);
    APP_ERROR_CHECK(err_code);
}

static void services_init(void)
{/// init user primary services
    ble_pss_init_t psystem_init;

    // init system primary service
    memset(&psystem_init, 0, sizeof(psystem_init));							
    psystem_init.evt_handler          = NULL;
    psystem_init.support_notification = true;
    psystem_init.p_report_ref         = NULL;
    initBLESEN((ble_pss_t *)&m_sys_s, &psystem_init, BLE_SYSTEM_SERVICE_UUID);
    initBLESEN((ble_pss_t *)&m_auth_s, &psystem_init, BLE_AUTHORIZATION_SERVICE);
   
}

static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{/// handle Connection Parameters Module events
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}

static void conn_params_error_handler(uint32_t nrf_error)
{/// Connection Parameters error handler
    APP_ERROR_HANDLER(nrf_error);
}

static void conn_params_init(void)
{/// init Connection Parameters module
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}

static void advertising_start(void)
{/// start advertising
    uint32_t             err_code;
    ble_gap_adv_params_t adv_params;

    // Start advertising
    memset(&adv_params, 0, sizeof(adv_params));

    adv_params.type        = BLE_GAP_ADV_TYPE_ADV_IND;
    adv_params.p_peer_addr = NULL;
    adv_params.fp          = BLE_GAP_ADV_FP_ANY;
    adv_params.interval    = APP_ADV_INTERVAL;
    adv_params.timeout     = APP_ADV_TIMEOUT_IN_SECONDS;

    err_code = sd_ble_gap_adv_start(&adv_params);
    APP_ERROR_CHECK(err_code);
}

static void on_ble_evt(ble_evt_t * p_ble_evt)
{/// handle Application's BLE stack events
    uint32_t err_code;

    switch (p_ble_evt->header.evt_id)
    {
        case (BLE_GAP_EVT_CONNECTED):
        {
#ifdef NRF52_SYSTEM_DEBUG	
			printUART0("-> BLE: Device connected\n",0);
#endif
		
			
			
			g_ble_conn = (BLE_CONNECTED);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;

            break;
		}
        case (BLE_GAP_EVT_DISCONNECTED):
        {
#ifdef NRF52_SYSTEM_DEBUG	
			printUART0("-> BLE: Device disconnected\n",0);
#endif
			turnOffLED();
				 initAuthorizationsKeys();
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
			g_ble_conn = (BLE_NOT_CONNECTED);
			
            advertising_start();
            break;
		}
        case (BLE_GAP_EVT_SEC_PARAMS_REQUEST):
        {// Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle,
                                                   BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP,
                                                   NULL,
                                                   NULL);
            APP_ERROR_CHECK(err_code);
            break;
		}
        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
        {// No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;
		}
		case(BLE_GATTS_EVT_HVC):
		{
			ble_gatts_evt_hvc_t * p_hvc = &p_ble_evt->evt.gatts_evt.params.hvc;
			
			if(p_hvc->handle == m_sen_s.char_accel_handles.value_handle)
			{// ACK received for ACCEL characteristics indications
				g_ble_indication_ack &= ~(BLE_INDICATION_ACCEL_ACK_WAITING);
			}
			
			if(p_hvc->handle == m_sen_s.char_pace_handles.value_handle)
			{// ACK received for PACE characteristics indications
				g_ble_indication_ack &= ~(BLE_INDICATION_PACE_ACK_WAITING);
			}
			
			if(p_hvc->handle == m_sen_s.char_ecg_handles.value_handle)
			{// ACK received for ECG characteristics indications
				g_ble_indication_ack &= ~(BLE_INDICATION_ECG_ACK_WAITING);
			}
			
			if(p_hvc->handle == m_sen_s.char_sum_handles.value_handle)
			{// ACK received for ECG characteristics indications
				g_ble_indication_ack &= ~(BLE_INDICATION_SUM_ACK_WAITING);
			}
			
			break;
		}
        default:
            // No implementation needed.
            break;
    }
}

static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{/// dispatching a BLE stack event to all modules
    on_ble_evt(p_ble_evt);
    ble_conn_params_on_ble_evt(p_ble_evt);
    
    onBleEvenBLESEN((ble_pss_t *)&m_sys_s, p_ble_evt);
}

static void ble_stack_init(void)
{/// BLE stack initialization
    uint32_t err_code;

    // Initialize the SoftDevice handler module.
    //SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_XTAL_20_PPM, false);
    SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_RC_250_PPM_1000MS_CALIBRATION, false);
    
    
    
    ble_enable_params_t ble_enable_params;
    err_code = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,
                                                    PERIPHERAL_LINK_COUNT,
                                                    &ble_enable_params);
    APP_ERROR_CHECK(err_code);
    
    //Check the ram settings against the used number of links
    CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT,PERIPHERAL_LINK_COUNT);
    
    // Enable BLE stack.
    err_code = softdevice_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);

    ble_gap_addr_t addr;

    err_code = sd_ble_gap_address_get(&addr);
    APP_ERROR_CHECK(err_code);
    err_code = sd_ble_gap_address_set(BLE_GAP_ADDR_CYCLE_MODE_NONE, &addr);
    APP_ERROR_CHECK(err_code);

    // Subscribe for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);
}

void chkBLE(void)
{/// power manager
    uint32_t err_code = sd_app_evt_wait();

    APP_ERROR_CHECK(err_code);
}

void initBLE(void)
{
    timers_init();
    ble_stack_init();
    gap_params_init();
    services_init();
    advertising_init();
    conn_params_init();

    // Start execution.
    advertising_start();
}

