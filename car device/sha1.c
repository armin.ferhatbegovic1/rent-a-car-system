#include "sha1.h"

volatile uint32_t tA;
volatile uint32_t tB;
volatile uint32_t tC;
volatile uint32_t tD;
volatile uint32_t tE;

void genSHA(uint8_t * vec1, uint8_t * vec2, uint8_t * g_sha_gen_hash)
{
	uint32_t tmp, tmp1,tmp2,ft,wtt,kt;
	uint8_t k, m;
	uint32_t wt[16];
	uint32_t data1[5];
	
	m = 0;
	for(k=0;k<5;k++)
	{
		data1[k] = ((uint32_t)vec1[m])<<24;
		m++;
		data1[k] = ((uint32_t)vec1[m])<<16;
		m++;
		data1[k] = ((uint32_t)vec1[m])<<8;
		m++;
		data1[k] = ((uint32_t)vec1[m]);
		m++;
	}
	
	
	tA = ((uint32_t)vec2[0])<<24;
	tA |= ((uint32_t)vec2[1])<<16;
	tA |= ((uint32_t)vec2[2])<<8;
	tA |= ((uint32_t)vec2[3]);
	tB = ((uint32_t)vec2[4])<<24;
	tB |= ((uint32_t)vec2[5])<<16;
	tB |= ((uint32_t)vec2[6])<<8;
	tB |= ((uint32_t)vec2[7]);
	tC = ((uint32_t)vec2[8])<<24;
	tC |= ((uint32_t)vec2[9])<<16;
	tC |= ((uint32_t)vec2[10])<<8;
	tC |= ((uint32_t)vec2[11]);
	tD = ((uint32_t)vec2[12])<<24;
	tD |= ((uint32_t)vec2[13])<<16;
	tD |= ((uint32_t)vec2[14])<<8;
	tD |= ((uint32_t)vec2[15]);
	tE = ((uint32_t)vec2[16])<<24;
	tE |= ((uint32_t)vec2[17])<<16;
	tE |= ((uint32_t)vec2[18])<<8;
	tE |= ((uint32_t)vec2[19]);
	
	
	for (k=0;k<5;k++)
	{
		wt[k]=data1[k];
		wt[k+5]=data1[k];
		wt[k+10]=data1[k];
	}
	wt[15]=data1[4];
	
	for(k=0;k<80;k++)
	{
		
		tmp1=tA>>27;
		tmp2=tA<<5;
		tmp2|=tmp1;
		
		if(k<20)
		{
			ft=(tB&tC)|((~tB)&tD); 
		}
		else if((k>=20) && (k<40))
		{
			ft=tB^tC^tD;
		} 
		else if((k>=40) && (k<60))
		{
			ft=(tB&tC)|(tB&tD)|(tC&tD); 
		} 
		else 
		{
			ft=tB^tC^tD;
		}
		
		if(k<16)
		{
			wtt=wt[k]; 
		} 
		else
		{
			wtt=wt[13]^wt[8]^wt[2]^wt[0];
			if(wtt&0x80000000)
			{
				wtt=wtt<<1;
				wtt|=0x00000001;
			} 
			else
			{
				wtt=wtt<<1;
			}
			
			wt[0]=wt[1];
			wt[1]=wt[2];
			wt[2]=wt[3];
			wt[3]=wt[4];
			wt[4]=wt[5];
			wt[5]=wt[6];
			wt[6]=wt[7];
			wt[7]=wt[8];
			wt[8]=wt[9];
			wt[9]=wt[10];
			wt[10]=wt[11];
			wt[11]=wt[12];
			wt[12]=wt[13];
			wt[13]=wt[14];
			wt[14]=wt[15];
			wt[15]=wtt;
			
		}
	
		if(k==0) 
		{
			kt=0x5A827999;
		} 
		else if(k==20)
		{
			kt=0x6ED9EBA1;
		}
		else if(k==40)
		{
			kt=0x8F1BBCDC;
		}
		else if(k==60)
		{
			kt=0xCA62C1D6;
		}		
		
		tmp=tmp2+ft+wtt+kt+tE;
		
		tE=tD;
		tD=tC;		
		tC=(tB>>2)|(tB<<30);
		tB=tA;
		tA=tmp;
		
	}	
	
	
	g_sha_gen_hash[0] = (uint8_t)((tA>>24)&0x000000FF);
	g_sha_gen_hash[1] = (uint8_t)((tA>>16)&0x000000FF);
	g_sha_gen_hash[2] = (uint8_t)((tA>>8)&0x000000FF);
	g_sha_gen_hash[3] = (uint8_t)(tA&0x000000FF);
	g_sha_gen_hash[4] = (uint8_t)((tB>>24)&0x000000FF);
	g_sha_gen_hash[5] = (uint8_t)((tB>>16)&0x000000FF);
	g_sha_gen_hash[6] = (uint8_t)((tB>>8)&0x000000FF);
	g_sha_gen_hash[7] = (uint8_t)(tB&0x000000FF);
	g_sha_gen_hash[8] = (uint8_t)((tC>>24)&0x000000FF);
	g_sha_gen_hash[9] = (uint8_t)((tC>>16)&0x000000FF);
	g_sha_gen_hash[10] = (uint8_t)((tC>>8)&0x000000FF);
	g_sha_gen_hash[11] = (uint8_t)(tC&0x000000FF);
	g_sha_gen_hash[12] = (uint8_t)((tD>>24)&0x000000FF);
	g_sha_gen_hash[13] = (uint8_t)((tD>>16)&0x000000FF);
	g_sha_gen_hash[14] = (uint8_t)((tD>>8)&0x000000FF);
	g_sha_gen_hash[15] = (uint8_t)(tD&0x000000FF);
	g_sha_gen_hash[16] = (uint8_t)((tE>>24)&0x000000FF);
	g_sha_gen_hash[17] = (uint8_t)((tE>>16)&0x000000FF);
	g_sha_gen_hash[18] = (uint8_t)((tE>>8)&0x000000FF);
	g_sha_gen_hash[19] = (uint8_t)(tE&0x000000FF);
	
}

uint8_t chkHASH(uint8_t*g_sha_rx_hash,uint8_t*g_sha_gen_hash)
{/// check if received hash matches the calculated value
/*	uint8_t k, flag = 0;
	
	for(k=0;k<(BLE_CHAR_SIZE_HASH);k++)
	{
		if(g_sha_rx_hash[k] != g_sha_gen_hash[k])
		{
			flag = 1;
			break;
		}
	}
	
	if(flag == 0)
	{
#ifdef SYSTEM_DEBUG
		printUART0("-> SHA: access granted\n");
#endif
		// calculate new hash value and write message digest into Random Number 2 char
		genSHA((uint8_t *)g_sha_rnum1, (uint8_t *)g_sha_rx_hash);
		
		for(k=0;k<(BLE_CHAR_SIZE_RANDOM_NUM1);k++)
		{
			g_sha_rnum2[k] = g_sha_gen_hash[k];
		}
			
		// access enabled
		g_security_check[0] = 0x5A;
		
		return (HASH_VALIDATION_OK);
	}
	else
	{
#ifdef SYSTEM_DEBUG
		printUART0("-> SHA: validation failed\n");
#endif
		return (HASH_VALIDATION_ERROR);
	}*/
}

void getRNG(uint8_t * result, uint8_t size)
{/// get 20 Byte random number
	uint8_t k;
	
	NRF_RNG->CONFIG = 0x00000001;										// enable bias (uniform distribution)
	
	for(k=0;k<size;k++)
	{
		NRF_RNG->TASKS_START = 1;
		
		while(NRF_RNG->EVENTS_VALRDY == 0);
		
		NRF_RNG->TASKS_STOP = 1;
		NRF_RNG->EVENTS_VALRDY = 0;
		
		result[k] = NRF_RNG->VALUE;
	}	
}
