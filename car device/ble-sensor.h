#ifndef _BLE_PHY_H_
#define _BLE_PHY_H_

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"

#include "nrf52.h"
#include "nrf_sdm.h"
#include "ble_gatt.h"
#include "ble_gatts.h"

#include "battery.h"
#include "config.h"



#define BLE_APPEARANCE_PHYSICS_SENSOR								2015
///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
/// Phy sensor structures
///---------------------------------------------------------------------<
typedef enum
{
    BLE_PSS_EVT_NOTIFICATION_ENABLED,                            		// notification enabled event
    BLE_PSS_EVT_NOTIFICATION_DISABLED                            		// notification disabled event
} ble_pss_evt_type_t;

typedef struct
{
    ble_pss_evt_type_t evt_type;                                  		// type of event
} ble_pss_evt_t;

// Forward declaration of the ble_pss_t type. 
typedef struct ble_pss_s ble_pss_t;

typedef void (*ble_pss_evt_handler_t) (ble_pss_t * p_pss, ble_pss_evt_t * p_evt);

typedef struct
{
    ble_pss_evt_handler_t         evt_handler;                    		// event handler 
    bool                          support_notification;          		// true if notification is supported
    ble_srv_report_ref_t *        p_report_ref;                   		// if not NULL, a Report Reference descriptor with the specified value will be added tocharacteristic 
    ble_srv_cccd_security_mode_t  phy_sens_char_attr_md;     			// Initial security level for physim characteristics attribute 
    ble_gap_conn_sec_mode_t       phy_sens_report_read_perm; 			// Initial security level for physim report read attribute 
} ble_pss_init_t;


typedef struct ble_pss_s												// various status information for the service
{
    ble_pss_evt_handler_t         evt_handler;                    		// event handler
    uint16_t                      service_handle;               		// handle of physim Service
    ble_gatts_char_handles_t      char_accel_handles;          			// handles related accel characteristic
    ble_gatts_char_handles_t      char_pace_handles;          			// handles related pace characteristic
    ble_gatts_char_handles_t      char_ecg_handles;          			// handles related ecg characteristic
    ble_gatts_char_handles_t      char_temp_handles;          			// handles related temp characteristic
    ble_gatts_char_handles_t      char_sum_handles;          			// handles related temp characteristic
    ble_gatts_char_handles_t        char_auth_key_1_handle;
    ble_gatts_char_handles_t        char_auth_key_2_handle;
    ble_gatts_char_handles_t         char_auth_hash_handle;
  
    uint16_t                      report_ref_handle;              		// handle of the Report Reference descriptor.
    uint16_t                      conn_handle;                    		// handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection). 
    bool                          is_notification_supported;      		// true if notification is supported.
    uint8_t                    	  uuid_type;
} ble_pss_t;




#define BLE_CHAR_DESC_ARRAY_SIZE					32

#define BLE_CHAR_TYPE_BAT_LVL						0
#define BLE_CHAR_TYPE_RTC							1
#define BLE_CHAR_TYPE_CHARGE						2
#define BLE_CHAR_TYPE_ACCEL							3
#define BLE_CHAR_TYPE_PACE							4
#define BLE_CHAR_TYPE_ECG							5
#define BLE_CHAR_TYPE_TEMP							6
#define BLE_CHAR_TYPE_SUM							7

#define BLE_CHAR_TYPE_AUTH_KEY_1					0x11
#define BLE_CHAR_TYPE_AUTH_KEY_2					0x22
#define BLE_CHAR_TYPE_AUTH_HASH  					0x33

#define	BLE_CHAR_TYPE_AUTH_KEY_1_UUID				 	0x20CA
#define	BLE_CHAR_TYPE_AUTH_KEY_2_UUID				 	0x20CD
#define	BLE_CHAR_TYPE_AUTH_HASH_UUID				 	0x20CE

#define BLE_DEVICE_HASH_RECEIVED			            11	
#define BLE_DEVICE_NOT_AUTHORIZED 						22
#define BLE_DEVICE_AUTHORIZED                 		    33
#define HASH_VALIDATION_OK								44
#define HASH_VALIDATION_BAD								55


#define BLE_UUID_BASE {0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
#define BLE_SYSTEM_SERVICE_UUID			 		0x2000
#define BLE_AUTHORIZATION_SERVICE_UUID 			0x2009
#define BLE_DOWNLOAD_SERVICE_UUID	 			0x2010

#define BLE_SENSOR_CHAR_BAT_LVL					0x2E00
#define BLE_SENSOR_CHAR_RTC						0x2E08
#define BLE_SENSOR_CHAR_BAT_CHARGE_STATE		0x2E10
#define BLE_SENSOR_CHAR_ACCEL					0x2E18
#define BLE_SENSOR_CHAR_PACE					0x2E20
#define BLE_SENSOR_CHAR_ECG						0x2E28
#define BLE_SENSOR_CHAR_EXTEMP					0x2E30
#define BLE_SENSOR_CHAR_SUM						0x2E38

		
#define	BLE_SYSTEM_SERVICE						0
#define	BLE_AUTHORIZATION_SERVICE				1
#define	BLE_DOWNLOAD_SERVICE					2


#define BLE_PROPERTISE_READ						0x01
#define BLE_PROPERTISE_WRITE					0x02
#define BLE_PROPERTISE_INDICATION				0x04					// has ACK's 
#define BLE_PROPERTISE_NOTIFICATION				0x08					// no ACK's

#define BLE_INDICATION_NONE						0x00					// there are not indications active
#define BLE_INDICATION_ACCEL					0x01					// stream accel data
#define BLE_INDICATION_PACE						0x02					// stream pace data
#define BLE_INDICATION_ECG						0x04					// stream ecg data
#define BLE_INDICATION_SUM						0x08

#define BLE_DOWNLOAD_AES_CBC_BLOCK_SIZE			64
#define BLE_DOWNLOAD_AES_PAGE_DATA_SIZE			(((BLE_DOWNLOAD_AES_CBC_BLOCK_SIZE)/4) + 1)
#define BLE_AES_BUFFER_EMPTY					0
#define BLE_AES_BUFFER_FULL						1

#define BLE_CONNECTED							1
#define BLE_NOT_CONNECTED						0

#define BLE_TX_TYPE_ACCEL						0
#define BLE_TX_TYPE_PACE						1
#define BLE_TX_TYPE_ECG							2
#define BLE_TX_TYPE_SUM							3

#define BLE_INDICATION_ALL_ACK_RECEIVED			0x00
#define BLE_INDICATION_ACCEL_ACK_WAITING		0x01
#define BLE_INDICATION_PACE_ACK_WAITING			0x02
#define BLE_INDICATION_ECG_ACK_WAITING			0x04
#define BLE_INDICATION_SUM_ACK_WAITING			0x08


uint32_t 	initBLESEN(ble_pss_t * p_pss, const ble_pss_init_t * p_pss_init, uint8_t type);
uint32_t 	addCharBLESEN(ble_pss_t * p_pss, const ble_pss_init_t * p_pss_init, uint8_t propertise, uint8_t type);
void 		getCharDescBLESEN(uint8_t * user_desc, uint8_t type);
uint16_t 	getUuidBLESEN(uint8_t type);
uint8_t * 	getDataPtrBLESEN(uint8_t type);
uint16_t 	getDataLenBLESEN(uint8_t type);
void 		chk4TxBLESEN(void);

void 		onBleEvenBLESEN(ble_pss_t * p_pss, ble_evt_t * p_ble_evt);
void 		onWriteBLESEN(ble_pss_t * p_pss, ble_evt_t * p_ble_evt);
void 		onDiscBLESEN(ble_pss_t * p_pss, ble_evt_t * p_ble_evt);
void 		onConnBLESEN(ble_pss_t * p_pss, ble_evt_t * p_ble_evt);
uint32_t 	txDataBLESEN(ble_pss_t * p_pss, uint8_t dtype, uint16_t len);
void 		updateRTCValBLESEN(void);
void 		updateBatLvlBLESEN(uint8_t bat_lvl);
ble_gatts_char_handles_t * getCharHandleBLESCF(uint8_t type);

extern volatile uint8_t 	g_ble_conn;
extern volatile uint8_t 	g_ble_indication_ack;


extern volatile ble_pss_t                        m_sys_s; 
extern volatile ble_pss_t                        m_sen_s; 
extern volatile ble_pss_t                        m_auth_s; 

extern volatile uint8_t g_ble_sum_data[BLE_CHAR_SUM_ARRAY_SIZE];
#endif 

