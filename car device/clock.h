#ifndef __CLOCK_H_
#define __CLOCK_H_

#include <stdint.h>
#include "nrf52.h"
#include "config.h"

#define RTC_TIMEOUT				0
#define RTC_KEEP_ALIVE			1

#define CLOCK_HF				0
#define CLOCK_LF				1

void 		startCLOCK(uint8_t clock);

void 		delay_ms(uint32_t ms);
void		delay_us(uint32_t us);
void 		startSWATCH(void);
uint32_t 	stopSWATCH(void);

void 		initRTC2(void);
uint8_t 	chk4TimeoutRTC2(uint32_t t_beg, uint32_t t_period);
uint32_t 	getRTC2(void);


#endif
