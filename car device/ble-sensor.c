#include "ble-sensor.h"
#include <string.h>
#include "nordic_common.h"
#include "ble_srv_common.h"
#include "app_util.h"
#include "authorization.h"


///_____________________________________________________________________
///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
/// nRF52 BLE System Service - variables
///---------------------------------------------------------------------
		
///_____________________________________________________________________
///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
/// nRF52 BLE Sensor Service - variables
///---------------------------------------------------------------------

///_____________________________________________________________________
///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

volatile ble_pss_t                        	m_sys_s; 
volatile ble_pss_t                          m_auth_s;
volatile ble_pss_t                        	m_sen_s; 
volatile ble_pss_t							m_dow_s; 

volatile uint8_t g_ble_conn = (BLE_NOT_CONNECTED);
volatile uint8_t g_ble_indications = (BLE_INDICATION_NONE);
volatile uint8_t g_ble_indication_ack = (BLE_INDICATION_ALL_ACK_RECEIVED);
volatile uint8_t g_ble_sum_data[BLE_CHAR_SUM_ARRAY_SIZE];

volatile uint8_t g_ble_security_flag = 0;

uint32_t initBLESEN(ble_pss_t * p_pss, const ble_pss_init_t * p_pss_init, uint8_t type)
{ /// init BLE physical sensor service
	uint8_t ble_char_propertise;
    ble_uuid_t ble_uuid;
	ble_uuid128_t base_uuid = {BLE_UUID_BASE};

	//wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    // initialize service structure
    //wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    p_pss->evt_handler               = p_pss_init->evt_handler;			// set event handler
    p_pss->conn_handle               = BLE_CONN_HANDLE_INVALID;			// connection status
    p_pss->is_notification_supported = p_pss_init->support_notification;// notifications enabled
    
   	
   	if(type == (BLE_SYSTEM_SERVICE))
	{
		//wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
		// add device system service
		//wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
		sd_ble_uuid_vs_add(&base_uuid, &p_pss->uuid_type);					// add a vendor specific UUID to the BLE stack's table,
																			// so that we can later on use short UUID's
		ble_uuid.type = p_pss->uuid_type;									// copy BLE UUID type (can be primary or secondary)
		ble_uuid.uuid = (BLE_SYSTEM_SERVICE_UUID);							// set BLE primary service short 16-bit UUID
		
		sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &p_pss->service_handle); // add primary service
		//wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
		// add service BLE characteristics: read data 
		//wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
		ble_char_propertise = (BLE_PROPERTISE_READ)|(BLE_PROPERTISE_WRITE);
		addCharBLESEN(p_pss, p_pss_init, ble_char_propertise, BLE_CHAR_TYPE_BAT_LVL);
		
		//ble_char_propertise = (BLE_PROPERTISE_READ);
		//addCharBLESEN(p_pss, p_pss_init, ble_char_propertise, BLE_CHAR_TYPE_RTC);
		
		//ble_char_propertise = (BLE_PROPERTISE_READ);
		//addCharBLESEN(p_pss, p_pss_init, ble_char_propertise, BLE_CHAR_TYPE_CHARGE);
		
	} 
	else if(type == (BLE_AUTHORIZATION_SERVICE))
	{
		////wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
		//// add sensor data service
		////wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
		sd_ble_uuid_vs_add(&base_uuid, &p_pss->uuid_type);					// add a vendor specific UUID to the BLE stack's table,
																			//// so that we can later on use short UUID's
		ble_uuid.type = p_pss->uuid_type;									// copy BLE UUID type (can be primary or secondary)
		ble_uuid.uuid = (BLE_AUTHORIZATION_SERVICE_UUID);							// set BLE primary service short 16-bit UUID
		
		sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &p_pss->service_handle); // add primary service
		
		////wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
		//// add BLE characteristics: read and indications
		////wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
		ble_char_propertise = (BLE_PROPERTISE_READ);
		addCharBLESEN(p_pss, p_pss_init, ble_char_propertise, BLE_CHAR_TYPE_AUTH_KEY_1);

	    ble_char_propertise = (BLE_PROPERTISE_READ);
		addCharBLESEN(p_pss, p_pss_init, ble_char_propertise, BLE_CHAR_TYPE_AUTH_KEY_2);
    
        ble_char_propertise = (BLE_PROPERTISE_WRITE);
		addCharBLESEN(p_pss, p_pss_init, ble_char_propertise, BLE_CHAR_TYPE_AUTH_HASH);
		
	}  
	
}

uint32_t addCharBLESEN(ble_pss_t * p_pss, const ble_pss_init_t * p_pss_init, uint8_t propertise, uint8_t type)
{/// add phy sensor characteristics
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    
    uint8_t user_desc[BLE_CHAR_DESC_ARRAY_SIZE];
    
    getCharDescBLESEN(user_desc, type);									// get characteristic description 
   
    //wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    // Set security mode for characteristic
    //wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
	memset(&cccd_md, 0, sizeof(cccd_md));
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);					// set sec mode to require no protection, open link. 
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);				// set sec mode to require no protection, open link. 
	cccd_md.vloc = BLE_GATTS_VLOC_STACK;
    
    //wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    // Set characteristic parameters: propertise, descriptor and security 
    //wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read = 0; 	
    char_md.char_props.write = 0;
    char_md.char_props.indicate = 0;
    char_md.char_props.notify = 0;
    
    if(propertise &(BLE_PROPERTISE_READ))
    {
		char_md.char_props.read   = 1; 									// set char propertise -> enable read
	}
	if(propertise &(BLE_PROPERTISE_WRITE))
    {
		char_md.char_props.write   = 1;									// set char propertise -> enable write
	}
	if(propertise &(BLE_PROPERTISE_INDICATION))
    {
		char_md.char_props.indicate = 1;								// set char propertise -> enable indications
	}
	if(propertise &(BLE_PROPERTISE_NOTIFICATION))
    {
		char_md.char_props.notify = 1;									// set char propertise -> enable notifications
	}
    

    char_md.p_user_desc_md    = NULL;									// attribute metadata for the User Description descriptor 
    char_md.p_cccd_md         = &cccd_md;								// attribute metadata for the Client Characteristic Configuration Descriptor,
    char_md.p_sccd_md         = NULL;									// attribute metadata for the Server Characteristic Configuration Descriptor 
    
    char_md.p_char_user_desc  = user_desc;								// user descriptor
    char_md.char_user_desc_size = strlen((char *)user_desc);					
    char_md.char_user_desc_max_size = strlen((char *)user_desc);
    
    //wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    // Set BLE UUID type 
    //wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    ble_uuid.type = p_pss->uuid_type;
    ble_uuid.uuid = getUuidBLESEN(type);
    
    //wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    // 
    //wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    memset(&attr_md, 0, sizeof(attr_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);					// enable read on charatericstic value
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);				// enable write on characteristic value
    attr_md.vloc       = BLE_GATTS_VLOC_USER;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0; 
 
    //wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    // Set attribute value
    //wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    attr_char_value.p_uuid       = &ble_uuid;							// set attribute UUID
    attr_char_value.p_attr_md    = &attr_md;							// set metadata
	attr_char_value.init_len     = getDataLenBLESEN(type);				// characteristic data len
	attr_char_value.init_offs    = 0;
	attr_char_value.max_len      = getDataLenBLESEN(type);				// characteristic data len
	attr_char_value.p_value      = getDataPtrBLESEN(type);				// pointer to data vector
	
	
	if(type == (BLE_CHAR_TYPE_AUTH_KEY_1))
	{
		return sd_ble_gatts_characteristic_add(p_pss->service_handle, &char_md,
												&attr_char_value,
												&p_pss->char_auth_key_1_handle);
	}else
	if(type == (BLE_CHAR_TYPE_AUTH_KEY_2))
	{
		return sd_ble_gatts_characteristic_add(p_pss->service_handle, &char_md,
												&attr_char_value,
												&p_pss->char_auth_key_2_handle);
	}
	if(type == (BLE_CHAR_TYPE_AUTH_HASH))
	{
		return sd_ble_gatts_characteristic_add(p_pss->service_handle, &char_md,
												&attr_char_value,
												&p_pss->char_auth_hash_handle);
	}
	else if(type == (BLE_CHAR_TYPE_PACE))
	{
		return sd_ble_gatts_characteristic_add(p_pss->service_handle, &char_md,
												&attr_char_value,
												&p_pss->char_pace_handles);
	}
	else if(type == (BLE_CHAR_TYPE_ECG))
	{
		return sd_ble_gatts_characteristic_add(p_pss->service_handle, &char_md,
												&attr_char_value,
												&p_pss->char_ecg_handles);
	}
	else if(type == (BLE_CHAR_TYPE_SUM))
	{
		return sd_ble_gatts_characteristic_add(p_pss->service_handle, &char_md,
												&attr_char_value,
												&p_pss->char_sum_handles);
	}
	else
	{
		return sd_ble_gatts_characteristic_add(p_pss->service_handle, &char_md,
												&attr_char_value,
												&p_pss->char_temp_handles);
	}
}

uint8_t * getDataPtrBLESEN(uint8_t type)
{/// get given characteristic data pointer
	switch(type)
    {
		
		case(BLE_CHAR_TYPE_BAT_LVL):
		{
			return (uint8_t *)g_bat_lvl;
		}
		case(BLE_CHAR_TYPE_AUTH_KEY_1):
		{	
		return (uint8_t*)auth_key_1;
		}
		case(BLE_CHAR_TYPE_AUTH_KEY_2):
		{	
		return (uint8_t*)auth_key_2;
		}
		case(BLE_CHAR_TYPE_AUTH_HASH):
		{	
		return (uint8_t*)auth_hash;
		}
		
		
		default:
		{
			return NULL;
		}
	}
}

uint16_t getDataLenBLESEN(uint8_t type)
{/// get given characteristic data length
	switch(type)
    {
		
		case(BLE_CHAR_TYPE_BAT_LVL):
		{
			return (BLE_CHAR_BAT_LVL_ARRAY_SIZE);
		}
		case(BLE_CHAR_TYPE_AUTH_KEY_1):
		{
			return (AUTHORIZATION_KEY_SIZE);
		}
	    case(BLE_CHAR_TYPE_AUTH_KEY_2):
		{
			return (AUTHORIZATION_KEY_SIZE);
		}
		   case(BLE_CHAR_TYPE_AUTH_HASH):
		{
			return (AUTHORIZATION_KEY_SIZE);
		}
		default:
		{
			return 0;
		}
	}
}

uint16_t getUuidBLESEN(uint8_t type)
{/// get given characteristic UUID
	uint16_t uuid;
	
	switch(type)
    {
			
		case(BLE_CHAR_TYPE_BAT_LVL):
		{
			uuid = (BLE_SENSOR_CHAR_BAT_LVL);
			break;
		}
		case(BLE_CHAR_TYPE_AUTH_KEY_1):
		{
			uuid = (BLE_CHAR_TYPE_AUTH_KEY_1_UUID);
			break;
		}
			case(BLE_CHAR_TYPE_AUTH_KEY_2):
		{
			uuid = (BLE_CHAR_TYPE_AUTH_KEY_2_UUID);
			break;
		}
			case(BLE_CHAR_TYPE_AUTH_HASH):
		{
			uuid = (BLE_CHAR_TYPE_AUTH_HASH_UUID);
			break;
		}
		default:
		{
			break;
		}
	}
	
	return uuid;
}

void getCharDescBLESEN(uint8_t * user_desc, uint8_t type)
{/// copy characteristic description strings
	uint8_t k;
	for(k=0;k<(BLE_CHAR_DESC_ARRAY_SIZE);k++)
		user_desc[k] = 0x00;
	
	switch(type)
    {
		
		case(BLE_CHAR_TYPE_BAT_LVL):
		{
			strcpy((char *)user_desc, "Battery level [%]");
			break;
		}
		case(BLE_CHAR_TYPE_AUTH_KEY_1):
		{
			strcpy((char *)user_desc, "Auth Key 1");
			break;
		}
		case(BLE_CHAR_TYPE_AUTH_HASH):
		{
			strcpy((char *)user_desc, "Hash value");
			break;
		}
		case(BLE_CHAR_TYPE_AUTH_KEY_2):
		{
			strcpy((char *)user_desc, "Auth Key 2");
			break;
		}
		default:
		{
			break;
		}
	}
}


void onBleEvenBLESEN(ble_pss_t * p_pss, ble_evt_t * p_ble_evt)
{/// call functions for appropriate ble events
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            onConnBLESEN(p_pss, p_ble_evt);
            break;
            
        case BLE_GAP_EVT_DISCONNECTED:
            onDiscBLESEN(p_pss, p_ble_evt);
            break;
            
        case BLE_GATTS_EVT_WRITE:
            onWriteBLESEN(p_pss, p_ble_evt);
            break;
            
        default:
            // No implementation needed.
            break;
    }
}

void onConnBLESEN(ble_pss_t * p_pss, ble_evt_t * p_ble_evt)
{/// handle connect event
    p_pss->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
    m_sen_s.conn_handle = p_pss->conn_handle;
    
    g_ble_indications = (BLE_INDICATION_NONE);						// clear all download and stream pending actions
}

void onDiscBLESEN(ble_pss_t * p_pss, ble_evt_t * p_ble_evt)
{/// handle disconnect event
    UNUSED_PARAMETER(p_ble_evt);
    p_pss->conn_handle = (BLE_CONN_HANDLE_INVALID);
    m_sen_s.conn_handle = (BLE_CONN_HANDLE_INVALID);
    
    g_ble_indication_ack = (BLE_INDICATION_ALL_ACK_RECEIVED);
    g_ble_indications = (BLE_INDICATION_NONE);						// clear all download and stream pending actions
}

void onWriteBLESEN(ble_pss_t * p_pss, ble_evt_t * p_ble_evt)
{/// handle write event
	ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
	uint32_t utmp32;
	
	
#ifdef NRF52_SYSTEM_DEBUG
	/*printUART0("-> BLE: Accessing handle: [%x] ",p_evt_write->handle);	
	printUART0(" VAL: [%xb]\n",p_evt_write->data[0]);	
	
	 uint8_t k;
	    printUART0("-> AUTH HASH: [",0);
		for(k=0;k<(AUTHORIZATION_KEY_SIZE);k++)
		{
			printUART0("%xb",p_evt_write->data[k]);
		}
		   printUART0("]\n",0);*/
	
#endif


if(p_evt_write->handle == 0x0015)
	{
		strncpy(recieved_hash, p_evt_write->data, AUTHORIZATION_KEY_SIZE);
		g_ble_security_flag= BLE_DEVICE_HASH_RECEIVED;
	//	printUART0(" BLE_DEVICE_HASH_RECEIVED\n");
			
	}else{	
	///	printUART0("NOT BLE_DEVICE_HASH_RECEIVED \n");	
		}

}

void chkBLESCF(void)
{

		if(g_ble_security_flag == BLE_DEVICE_HASH_RECEIVED)
	{	
		printUART0(" BLE_DEVICE_HASH_RECEIVED\n");
		
		if(checkIsHashValidate() == (HASH_VALIDATION_OK))
		{
			turnOnLED();
			g_ble_security_flag = (BLE_DEVICE_AUTHORIZED);
			printUART0("BLE_DEVICE_AUTHORIZED \n");	
			turnOnLED();
		}
		else
		{
			turnOffLED();
			//disconnectBLEF();
			g_ble_security_flag = (BLE_DEVICE_NOT_AUTHORIZED);
			printUART0("BLE_DEVICE_NOT_AUTHORIZED \n");	
		}
	}
}










uint32_t txDataBLESEN(ble_pss_t * p_pss, uint8_t dtype, uint16_t len)
{/// tx data using on concrete BLE service and characteristic
	//ble_gatts_hvx_params_t hvx_params;
    uint32_t err_code = (NRF_SUCCESS);
	
	
	//if(g_ble_conn == (BLE_CONNECTED))
	//{
		//memset(&hvx_params, 0, sizeof(hvx_params));
		//hvx_params.type     = BLE_GATT_HVX_NOTIFICATION;					// max 20 bytes to transmit
				
		//switch(dtype)
		//{
			//case(BLE_TX_TYPE_ACCEL):
			//{
				//hvx_params.handle   = p_pss->char_accel_handles.value_handle;
				//hvx_params.p_len = &len;
				//hvx_params.p_data = g_mc3610_data;
				//err_code = sd_ble_gatts_hvx(p_pss->conn_handle, &hvx_params);
				
				//g_ble_indication_ack |= (BLE_INDICATION_ACCEL_ACK_WAITING);
				//break;
			//}
			//case(BLE_TX_TYPE_PACE):
			//{	
				//hvx_params.handle   = p_pss->char_pace_handles.value_handle;
				//hvx_params.p_len = &len;
				//hvx_params.p_data = g_ads1293_pace;
				//err_code = sd_ble_gatts_hvx(p_pss->conn_handle, &hvx_params);
				
				
				//g_ble_indication_ack |= (BLE_INDICATION_PACE_ACK_WAITING);
				//break;
			//}
			//case(BLE_TX_TYPE_ECG):
			//{	
				//hvx_params.handle   = p_pss->char_ecg_handles.value_handle;
				//hvx_params.p_len = &len;
				//hvx_params.p_data = g_ads1293_ecg;
				//err_code = sd_ble_gatts_hvx(p_pss->conn_handle, &hvx_params);
				
				//g_ble_indication_ack |= (BLE_INDICATION_ECG_ACK_WAITING);
				//break;
			//}
			//case(BLE_TX_TYPE_SUM):
			//{
				//hvx_params.handle   = p_pss->char_sum_handles.value_handle;
				//hvx_params.p_len = &len;
				//hvx_params.p_data = g_ble_sum_data;
				//err_code = sd_ble_gatts_hvx(p_pss->conn_handle, &hvx_params);
				
				//g_ble_indication_ack |= (BLE_INDICATION_SUM_ACK_WAITING);
			//}
			//default:
			//{
				//break;
			//}
		//}
	//}
	//else
	//{
		//err_code = NRF_ERROR_INVALID_STATE;
	//}
    

    return err_code;
}

void chk4TxBLESEN(void)
{/// checks if there is pending data to be sent using BLE
	uint8_t k, n;
	//if(g_ble_indications == (BLE_INDICATION_NONE))
	//{
		//return;
	//}
	//else
	//{
		
		//if(g_ble_indications & (BLE_INDICATION_SUM))
		//{
			//readDataMC3610();											// read accelerometer data
			//readADS1293();												// read PACE & ECG data
			
			//// add pace data 3x[2B]
			//n = 0;
			//for(k=0;k<6;k++)
				//g_ble_sum_data[n + k] = g_ads1293_pace[k];
			
			//// add ecg data 3x[3B]
			//n += k;
			//for(k=0;k<9;k++)
				//g_ble_sum_data[n + k] = g_ads1293_ecg[k];
			
			//// add accelerometer data 3x[12bit]
			//n += k;
			//g_ble_sum_data[n] = (g_mc3610_data[0]<<4)&0xF0;
			//g_ble_sum_data[n] |= (g_mc3610_data[1]>>4)&0x0F;
			
			//n++;
			//g_ble_sum_data[n] = (g_mc3610_data[1]&0x0F)|(g_mc3610_data[2]&0x0F);
			
			//n++;
			//g_ble_sum_data[n] = g_mc3610_data[3];
			
			//n++;
			//g_ble_sum_data[n] = (g_mc3610_data[4]<<4)&0xF0;
			//g_ble_sum_data[n] |= (g_mc3610_data[5]>>4)&0x0F;
			
			//n++;
			//g_ble_sum_data[n] = (g_mc3610_data[5]&0x0F);
			
			
			
			//txDataBLESEN(&m_sen_s, BLE_TX_TYPE_SUM, BLE_CHAR_SUM_ARRAY_SIZE);
			//return;
			
		//}
		
		
		//if(g_ble_indications & (BLE_INDICATION_ACCEL))
		//{// stream ACCELEROMETER data
			//// check if we have received ACK for this indication
			////if(g_ble_indication_ack & (BLE_INDICATION_ACCEL_ACK_WAITING))
				////return;

			//readDataMC3610();											// read accelerometer data
			//txDataBLESEN(&m_sen_s, BLE_TX_TYPE_ACCEL, BLE_CHAR_ACCEL_ARRAY_SIZE);
		//}
		
		//if((g_ble_indications & (BLE_INDICATION_PACE))||(g_ble_indications & (BLE_INDICATION_ECG)))
		//{		
			//readADS1293();												// read PACE & ECG data
			
			//if(g_ble_indications & (BLE_INDICATION_PACE))
			//{// stream PACE data
				//// check if we have received ACK for this indication
				////if(g_ble_indication_ack & (BLE_INDICATION_PACE_ACK_WAITING))
					////return;
					
				//txDataBLESEN(&m_sen_s, BLE_TX_TYPE_PACE, BLE_CHAR_PACE_ARRAY_SIZE);
			//}
			
			//if(g_ble_indications & (BLE_INDICATION_ECG))
			//{// stream ECG data
				
				//// check if we have received ACK for this indication
				////if(g_ble_indication_ack & (BLE_INDICATION_ECG_ACK_WAITING))
					////return;
					
				//txDataBLESEN(&m_sen_s, BLE_TX_TYPE_ECG, BLE_CHAR_ECG_ARRAY_SIZE);
			//}
		//}
		
	//}
}


