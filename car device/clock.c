#include "clock.h"

volatile uint32_t g_rtc_time = 0;

void startCLOCK(uint8_t clock)
{
	switch(clock)
	{
		case(CLOCK_HF):
		{/// start HF 16MHz clock for SoC PLL
			//NRF_CLOCK->XTALFREQ |= 0x00FF;
			//NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;							// start 16 MHz crystal oscillator 
			//NRF_CLOCK->TASKS_HFCLKSTART = 1;
			//while(NRF_CLOCK->EVENTS_HFCLKSTARTED == 0);					// wait for the external oscillator to start up
			
			break;
		}
		case(CLOCK_LF):
		{/// start LF 32768 Hz clock 
			//NRF_CLOCK->LFCLKSRC = 0x00000001;							// select LFCLK src as external crystal
			NRF_CLOCK->LFCLKSRC = 0x00000000;							// select LFCLK src as internal RC 
			//NRF_CLOCK->LFCLKSRC = 0x00000002;							// select LFCLK src SYNTH from HFCLK  
			NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;
			NRF_CLOCK->TASKS_LFCLKSTART = 1;
			while (NRF_CLOCK->EVENTS_LFCLKSTARTED == 0);				// wait for the external oscillator to start up
			
			break;
		}
		default:
		{
			break;
		}
	}
}

void initRTC2(void)
{/// init RTC2 to run from LFCLK , configured to run on basis of ~1kHz period is 1.007ms
	NRF_RTC2->PRESCALER = 0;
	NRF_RTC2->TASKS_CLEAR = 1;
   
	NRF_RTC2->CC[0] = 33;
	NRF_RTC2->INTENSET = 0x00010000;									// enable int on CC[0] match
	NRF_RTC2->EVTENSET = 0x00010000;									// enable event routing
	
	NVIC_EnableIRQ(RTC2_IRQn);
	NRF_RTC2->TASKS_START = 1;			
}

void RTC2_IRQHandler(void)
{
    if ((NRF_RTC2->EVENTS_COMPARE[0] != 0) && ((NRF_RTC2->INTENSET & 0x00010000) != 0)) 
    {
        NRF_RTC2->EVENTS_COMPARE[0] = 0;								// clear interrupt flag
        g_rtc_time++;
      
        NRF_RTC2->TASKS_CLEAR = 1;										// restart RTC2
        
    }
}

uint32_t getRTC2(void)
{
	return g_rtc_time;
}

uint8_t chk4TimeoutRTC2(uint32_t t_beg, uint32_t t_period)
{	
	uint32_t rtc_time = g_rtc_time;
	if(rtc_time >= t_beg)
	{
		if(rtc_time >= (t_beg + t_period))
			return (RTC_TIMEOUT);
		else
			return (RTC_KEEP_ALIVE);
	}
	else
	{
		uint32_t utmp32 = 0xFFFFFFFF - t_beg;
		if((rtc_time + utmp32) >= t_period)
			return (RTC_TIMEOUT);
		else
			return (RTC_KEEP_ALIVE);
	}
	
	return (RTC_KEEP_ALIVE);
}

void delay_ms(uint32_t ms)
{/// delay based on 32 bit TIMER4, max delay 2^32 ms
	uint32_t k;
	for(k=0;k<ms;k++)
	{
		delay_us(1000);
	}
}

void delay_us(uint32_t us)
{/// delay based on 32 bit TIMER4, max delay 2^32 us
	NRF_TIMER4->TASKS_CLEAR = 1;
	
	NRF_TIMER4->MODE = 0x0000;											// select counter mode
	NRF_TIMER4->PRESCALER = 0x0004; 									// 16 MHz clock, prescaler 16 -> 1us time step
	NRF_TIMER4->BITMODE = 0x0003;										// 32 bit timer mode
	
	NRF_TIMER4->CC[0] = us;
	NRF_TIMER4->SHORTS = 0x0001;
	NRF_TIMER4->TASKS_START = 1;
	
	while(NRF_TIMER4->EVENTS_COMPARE[0] == 0);
	NRF_TIMER4->EVENTS_COMPARE[0] = 0;
	NRF_TIMER4->TASKS_STOP = 1;
}

void startSWATCH(void)
{		
	NRF_TIMER3->MODE = 0x0000;  										// set timer mode
	NRF_TIMER3->TASKS_CLEAR = 1;               							// clear the task first to be usable for later
	NRF_TIMER3->PRESCALER = 0x0004;										// 16 MHz clock, prescaler 16 -> 1us time step
	NRF_TIMER3->BITMODE = 0x0003;										// set counter to 32 bit resolution
	NRF_TIMER3->CC[0] = 0xFFFFFFFF;
	
	NRF_TIMER3->EVENTS_COMPARE[0] = 0;	
	NRF_TIMER3->TASKS_START = 1;               							// start timer
}

uint32_t stopSWATCH(void)
{
	uint32_t time;
	NRF_TIMER3->TASKS_CAPTURE[0] = 1;
	time = NRF_TIMER3->CC[0];
	NRF_TIMER3->EVENTS_COMPARE[0] = 0;
	NRF_TIMER3->TASKS_STOP = 1;
	
	return time;
}

