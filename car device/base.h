#ifndef __BASE_H_
#define __BASE_H_

#include <stdint.h>
#include "nrf52.h"
#include "clock.h"
#include "uart.h"
#include "config.h"
#include "battery.h"
#include "ble-sensor.h"



#define BASE_SLOW_SENSOR_UPDATE_PERIOD			1000						//  in units of 1000ms
#define BASE_FAST_SENSOR_UPDATE_PERIOD			327						// in units of 1/32768, 327 -> 100ms				

void initBASE(void);
void updateSensorBASE(void);

#endif 
