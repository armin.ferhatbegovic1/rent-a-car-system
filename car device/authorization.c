#include "authorization.h"
#include "ble-sensor.h"


uint8_t auth_key_1[AUTHORIZATION_KEY_SIZE];
uint8_t auth_key_2[AUTHORIZATION_KEY_SIZE];
uint8_t auth_key_sha[AUTHORIZATION_KEY_SIZE];
uint8_t auth_hash[AUTHORIZATION_KEY_SIZE];
uint8_t recieved_hash[AUTHORIZATION_KEY_SIZE];

void initAuthorizationsKeys()
{
getRNG((uint8_t *)auth_key_1, AUTHORIZATION_KEY_SIZE);
getRNG((uint8_t *)auth_key_2, AUTHORIZATION_KEY_SIZE);
genSHA(auth_key_1,auth_key_2, auth_key_sha);
		 
     printAuthKey1();
     printAuthKey2();
     printGeneratedHash();
}



void printAuthKey1(){
	 uint8_t k;
	    printUART0("-> AUTH KEY1: [",0);
		for(k=0;k<(AUTHORIZATION_KEY_SIZE);k++)
		{
			printUART0("%xb",auth_key_1[k]);
		}
		   printUART0("]\n",0);
	}
void printAuthKey2(){
	 uint8_t k;
	    printUART0("-> AUTH KEY2: [",0);
		for(k=0;k<(AUTHORIZATION_KEY_SIZE);k++)
		{
			printUART0("%xb",auth_key_2[k]);
		}
		   printUART0("]\n",0);
	}
	
	void printGeneratedHash(){
	 uint8_t k;
	    printUART0("-> Generated Hash: [",0);
		for(k=0;k<(AUTHORIZATION_KEY_SIZE);k++)
		{
			printUART0("%xb",auth_key_sha[k]);
		}
		   printUART0("]\n",0);
	}
	
	uint8_t checkIsHashValidate(){
		uint8_t k=0;
		for(k=0;k<(AUTHORIZATION_KEY_SIZE);k++)
	      if (auth_key_sha[k] != recieved_hash[k]) 
            return HASH_VALIDATION_BAD; 

		return HASH_VALIDATION_OK;
		}
