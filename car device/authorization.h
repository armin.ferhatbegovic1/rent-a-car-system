#ifndef __AUTHORIZATION_H_
#define __AUTHORIZATIO_H_

#include <stdint.h>
#include "nrf52.h"
#include "misc.h"
#include "clock.h"
#include "sha1.h"

#define AUTHORIZATION_KEY_SIZE					8	

extern uint8_t auth_key_1[AUTHORIZATION_KEY_SIZE];
extern uint8_t auth_key_2[AUTHORIZATION_KEY_SIZE];
extern uint8_t auth_hash[AUTHORIZATION_KEY_SIZE];
extern uint8_t recieved_hash[AUTHORIZATION_KEY_SIZE];

void initAuthorizationsKeys();
void initAuthorizationsKey(int8_t*auth_key);
void printAuthKey1();
void printAuthKey2();
void printGeneratedHash();
uint8_t checkIsHashValidate();

/*
void initUART0(uint8_t tx, uint8_t rx, uint32_t baudrate);
extern volatile uint8_t g_uart_buff[UART_RX_BUFFER_SIZE];
*/
#endif 
