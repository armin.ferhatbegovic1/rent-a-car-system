import { AxiosInstance } from '../../services'
import { GET_ERRORS, SET_CURRENT_USER } from './types'
import { SetAuthToken } from '../../services/'

export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  }
}

export const registerUser = (user, history) => dispatch => {
  AxiosInstance.post('/v1/register', user)
  .then(res => history.push('/login'))
  .catch(err => {
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data
    })
  })
}



export const loginUser = (user) => dispatch => {
  AxiosInstance.post('/v1/login', user)
  .then(res => {
    console.log(' res.data')
    console.log(res.data)
    const {token} = res.data

    SetAuthToken(token)

    dispatch(setCurrentUser(user))
  })
  .catch(err => {
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data
    })
  })
}


export const logoutUser = (history) => dispatch => {
  localStorage.removeItem('jwtToken')
  SetAuthToken(false)
  dispatch(setCurrentUser({}))
  history.push('/signin')
}