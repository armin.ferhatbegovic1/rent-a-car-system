import { AxiosInstance } from '../../services'
import { GET_ERRORS, SET_CURRENT_USER } from './types'
import { SetAuthToken } from '../../services/'
import jwt_decode from 'jwt-decode'

export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  }
}

export const registerUser = (user, history) => dispatch => {
  AxiosInstance.post('/user/register', user)
  .then(res => history.push('/login'))
  .catch(err => {
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data
    })
  })
}

export const loginUser = (user) => dispatch => {
  AxiosInstance.post('/user/login', user)
  .then(res => {
    window.location.href = '/home'
    const {token} = res.data
    localStorage.setItem('jwtToken', token)
    SetAuthToken(token)
    const decoded = jwt_decode(token)
    dispatch(setCurrentUser(decoded))
  })
  .catch(err => {
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data
    })
  })
}

export const logoutUser = (history) => dispatch => {
  localStorage.removeItem('jwtToken')
  SetAuthToken(false)
  dispatch(setCurrentUser({}))
  history.push('/signin')
}