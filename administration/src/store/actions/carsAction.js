import * as actionTypes from './types';
import { getRequest, postRequest } from './actionTemplates';

export function getCars() {
  return async function action(dispatch) {
    const carsData = await getRequest('/cars');
    await dispatch({ type: actionTypes.SET_CARS_DATA, payload:carsData });
  };
}
