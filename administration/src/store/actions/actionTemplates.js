//import store from '../../index';
import apiEndpoint from '../../config/config'

export function getRequest (apiLocation) {

  return fetch(apiEndpoint.apiEndpoint + apiLocation, {
    method: 'GET',
     headers: { 'content-type': 'application/json', 'Authorization': localStorage.getItem('jwtToken') }
    //headers: {'content-type': 'application/json'}
  })
  .then(function (response) {
    console.log(response)
    return response.json()
  })
  .then(function (data) {
    /* if(data.authFail) {
         window.location.reload();
     }*/
    return data
  })
  .catch(function (error) {
    console.log('Error: ', error)
  })
}

export function postRequest (apiLocation, data, customFields) {
  /* return fetch(apiLocation, {
       method: 'POST',
       headers: { 'content-type': 'application/json', token: localStorage.getItem('user-token') },
       body: JSON.stringify(customFields ? data : { data })
   })
       .then(function(response) {
           return response.json();
       })
       .then(function(data) {
           if(data.authFail) {
               window.location.reload();
           }
           if(data.alert) {
               store.dispatch({ type: 'toast/OPEN', payload: true });
               store.dispatch({ type: 'toast/MESSAGE', payload: data.message });
               store.dispatch({ type: 'toast/TYPE', payload: data.success ? 'success' : 'failure' });
           }
           return data;
       })
       .catch(function(error) {
           console.log('Error: ', error);
       });*/
}
