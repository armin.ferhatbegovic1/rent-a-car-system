import * as actionTypes from './types'
import { getRequest } from './actionTemplates'

export function getReservations (filter) {
  return async function action (dispatch) {
    const reservationsData = await getRequest('/reservation/list-reservations/' + filter)
    await dispatch({type: actionTypes.SET_RESERVATIONS_DATA, payload: reservationsData})
  }
}