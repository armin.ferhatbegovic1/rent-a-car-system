import * as actionTypes from './types';
import { getRequest, postRequest } from './actionTemplates';

export function getUsers() {
  return async function action(dispatch) {
    const usersData = await getRequest('/users');
    await dispatch({ type: actionTypes.SET_USERS_DATA, payload:usersData });
  };
}
