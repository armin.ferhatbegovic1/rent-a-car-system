import settings from './settings'
import authReducer from './authReducer'
import userReducer from './userReducer'
import carReducer from './carReducer'
import reservationReducer from './reservationReducer'
import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import errorReducer from './errorReducer'

export default combineReducers({
  settings,
  cars: carReducer,
  auth: authReducer,
  form: formReducer,
  errors: errorReducer,
  users: userReducer,
  reservations: reservationReducer
})

