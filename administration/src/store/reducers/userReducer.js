import * as actionTypes from '../actions/types'

const initialState = {
  users: []
}

export default function(state = initialState, action) {
  switch (action.type) {
    case actionTypes.SET_USERS_DATA: {
      return {...state, users: action.payload}
    }

    default:
      return state
  }
};

