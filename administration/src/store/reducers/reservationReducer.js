import * as actionTypes from '../actions/types'

const initialState = {
  reservations: []
}

export default function(state = initialState, action) {
  switch (action.type) {
    case actionTypes.SET_RESERVATIONS_DATA: {
      return {...state, reservations: action.payload}
    }

    default:
      return state
  }
};

