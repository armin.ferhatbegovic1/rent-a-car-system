import * as actionTypes from '../actions/types'

const initialState = {
  cars: []
}

export default function(state = initialState, action) {
  switch (action.type) {
    case actionTypes.SET_CARS_DATA: {
      return {...state, cars: action.payload}
    }

    default:
      return state
  }
};

