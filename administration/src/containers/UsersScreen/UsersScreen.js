import React, { Component } from 'react'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import { localize } from '../../services/'
import TableData from '../../components/UI/Table/TableData'
import { getUsers } from '../../store/actions/usersAction'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core'

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto'
  },
  table: {
    minWidth: 700
  }
})

const headers = [
  {key: 'full_name', label: 'Full Name'},
  {key: 'email', label: 'Email'},
  {key: 'country', label: 'Country'},
  {key: 'dot', label: 'Date of Birth'}
]

class UsersScreen extends Component {

  async componentDidMount () {
    const {getUsersData} = this.props
    await getUsersData()
  }

  render () {
    const {classes, users} = this.props
    return (<div>
      <Card>
        <CardContent>
          <Typography variant='headline'>{localize('users')}</Typography>
          {users.length ? <TableData data={users} header={headers} classes={classes} /> : <div>Loading...</div>}
        </CardContent>
      </Card>
    </div>)
  }
}

const mapStateToProps = state => {
  return {
    users: state.users.users
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getUsersData: () => dispatch(getUsers())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(UsersScreen))

