import React, { Component } from 'react'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import FormGroup from '@material-ui/core/FormGroup'
import Typography from '@material-ui/core/Typography'
import Checkbox from '@material-ui/core/Checkbox'
import { localize } from '../../services/'
import TableData from '../../components/UI/Table/TableData'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core'
import { getReservations } from '../../store/actions/reservationsAction'
import FormControlLabel from '@material-ui/core/FormControlLabel'

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto'
  },
  table: {
    minWidth: 700
  }
})

const headers = [
  {key: 'reservation_id', label: 'ID'},
  {key: 'user', label: 'Customer', property: 'full_name'},
  {key: 'car', label: 'Car', property: 'name'},
  {key: 'start_date', label: 'Start Date'},
  {key: 'end_date', label: 'End Date'},
  {key: 'start_time', label: 'Start time'},
  {key: 'end_time', label: 'End time'}
]

class ReservationsScreen extends Component {
  state = {
    allReservations: true,
    activeReservations: false,
    finishedReservations: false
  }

  async getReservationsData (filterParamtere) {
    const {getReservationsData} = this.props
    await getReservationsData(filterParamtere)
  }

  async componentDidMount () {
    await this.getReservationsData()
  }

  handleChange = name => event => {
    const checked = event.target.checked === true ? event.target.checked : !event.target.checked
    this.setState({[name]: checked})
    Object.keys(this.state).map((key) => {
      if (key !== name) {
        this.setState({[key]: false})
      }
    })
    this.getReservationsData(name)
  }

  render () {
    const {classes, reservations} = this.props

    return (<div>
      <Card>
        <CardContent>
          <Typography variant='headline'>{localize('reservations')}</Typography>
          <FormGroup row>
            <FormControlLabel
              control={
                <Checkbox
                  checked={this.state.allReservations}
                  onChange={this.handleChange('allReservations')}
                  color='primary'
                />
              }
              label={localize('all')}
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={this.state.activeReservations}
                  onChange={this.handleChange('activeReservations')}
                  color='primary'
                />
              }
              label={localize('active')}
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={this.state.finishedReservations}
                  onChange={this.handleChange('finishedReservations')}
                  color='primary'
                />
              }
              label={localize('finished')}
            />
          </FormGroup>
          {reservations.length ? <TableData data={reservations} header={headers} classes={classes} /> :
            <div>Loading...</div>}
        </CardContent>
      </Card>
    </div>)
  }
}

const mapStateToProps = state => {
  return {
    reservations: state.reservations.reservations
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getReservationsData: (filter) => {
      dispatch(getReservations(filter))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ReservationsScreen))

