import React from 'react'
import Button from '@material-ui/core/Button'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import { insertNewCar } from '../../services/Server/Server'
import { localize } from '../../services/'
export default class NewCarForm extends React.Component {

  state = {
    name: '',
    model: ''
  }

  handleChangeName = (event) => {
    const name = event.target.value
    this.setState({name})
  }
  handleChangeModel = (event) => {
    const model = event.target.value
    this.setState({model})
  }

  handleSubmit = () => {
    const {name, model} = this.state
    const {closeModal, save} = this.props
    const carData = {"name":name, "model":model}
    save(carData)
    closeModal()
  }

  render () {
    const {name, model} = this.state

    return (
      <ValidatorForm
        ref='form'
        onSubmit={this.handleSubmit}
        onError={errors => console.log(errors)}
      >
        <TextValidator
          label={localize('name')}
          onChange={this.handleChangeName}
          name='name'
          value={name}
          validators={['minStringLength:5']}
          errorMessages={['5 char min']}
        />
        <TextValidator
          label={localize('model')}
          onChange={this.handleChangeModel}
          name='model'
          value={model}
          validators={['minStringLength:5']}
          errorMessages={['5 char min']}
        />
        <Button type='submit'>{localize('save')}</Button>
      </ValidatorForm>
    )
  }
}