import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import config from '../../config'
import MenuListActions from './MenuListActions'
import Button from '@material-ui/core/Button'
import connect from 'react-redux/es/connect/connect'
import AddNewCar from './AddNewCar'
import { insertNewCar } from '../../services/Server/Server'
import { localize } from '../../services/'

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto'
  },
  table: {
    minWidth: 700
  }
})

class CarsTable extends React.Component {
  constructor (props) {
    super(props)
    this.child = React.createRef()
    this.state = {
      cars: [],
      headers: [
        {key: 'car_id', label: 'Car ID'},
        {key: 'name', label: 'Name'},
        {key: 'model', label: 'Model'},
        {key: 'action', label: 'Action'}
      ],
      addNewCarModalOpen: false
    }
  }

  componentDidMount () {
    this.listAllCars()
  }

  listAllCars () {
    const {apiEndpoint} = config
    fetch(apiEndpoint + 'cars')
    .then(response => {
      return response.json()
    }).then(result => {
      console.log(result)
      this.setState({
        cars: result
      })
    })
  }

  componentWillUpdate () {
    // this.listAllCars()
  }

  addNewCar = () => {
    this.setState({addNewCarModalOpen: true})
  }

  saveNewCar = () => {
    this.setState({addNewCarModalOpen: false})
  }

  render () {

    const {classes} = this.props
    const {cars, headers, addNewCarModalOpen} = this.state
    return (
      <Paper className={classes.root}>
        <Button onClick={this.addNewCar}>{localize('addNewCar')}</Button>
        <AddNewCar open={addNewCarModalOpen} save={insertNewCar} close={this.saveNewCar} />
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              {headers.map(function (row) {
                return (<TableCell key={row.key}>{row.label}</TableCell>)
              })}
            </TableRow>
          </TableHead>
          <TableBody>
            {cars.map(row => {
              return (
                <TableRow key={row.car_id}>
                  <TableCell component='th' scope='row'>
                    {row.name}
                  </TableCell>
                  <TableCell align='right'>{row.name}</TableCell>
                  <TableCell align='right'>{row.model}</TableCell>
                  <TableCell align='right'><MenuListActions /></TableCell>
                </TableRow>
              )
            })}
          </TableBody>
        </Table>
      </Paper>
    )
  }
}

CarsTable.propTypes = {
  classes: PropTypes.object.isRequired
}

export default connect(
  null
)(withStyles(styles)(CarsTable))

