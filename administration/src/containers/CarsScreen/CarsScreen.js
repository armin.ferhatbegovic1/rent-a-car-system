import React, { Component } from 'react'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import { localize } from '../../services/'
import TableData from '../../components/UI/Table/TableData'
import { getCars } from '../../store/actions/carsAction'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core'

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto'
  },
  table: {
    minWidth: 700
  }
})

const headers = [
  {key: 'car_id', label: 'Car ID'},
  {key: 'name', label: 'Name'},
  {key: 'model', label: 'Model'}
]

class CarsScreen extends Component {

  async componentDidMount () {
    const {getCarsData} = this.props
    await getCarsData()
  }

  render () {
    const {classes, cars} = this.props
    return (<div>
      <Card>
        <CardContent>
          <Typography variant='headline'>{localize('cars')}</Typography>
          {cars.length ? <TableData data={cars} header={headers} classes={classes} /> : <div>Loading...</div>}
        </CardContent>
      </Card>r
    </div>)
  }
}

const mapStateToProps = state => {
  return {
    cars: state.cars.cars
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getCarsData: () => dispatch(getCars())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(CarsScreen))

