import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Modal from '@material-ui/core/Modal'
import connect from 'react-redux/es/connect/connect'
import NewCarForm from './NewCarForm'

function getModalStyle () {

  return {
    top: `50%`,
    left: `50%`,
    transform: `translate(-50%, -50%)`
  }
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4
  }
})

class AddNewCarModal extends React.Component {

  handleClose = () => {

  }

  render () {
    const {classes, open, close, literals, save} = this.props
    return (
      <div>
        <Modal
          aria-labelledby='simple-modal-title'
          aria-describedby='simple-modal-description'
          open={open}
          onClose={this.handleClose}
        >

          <div style={getModalStyle()} className={classes.paper}>
            <NewCarForm literals={literals} closeModal={() => {close()}} save={save} />

          </div>
        </Modal>
      </div>
    )
  }
}

AddNewCarModal.propTypes = {
  classes: PropTypes.object.isRequired
}
const mapStateToProps = (state) => {
  return {
    literals: state.literals
  }
}

export default connect(
  mapStateToProps
)(withStyles(styles)(AddNewCarModal))
