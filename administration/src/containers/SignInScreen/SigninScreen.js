import React, { Component } from 'react'
import Card from '@material-ui/core/Card'
import { withStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import { loginUser } from '../../store/actions/loginActions'
import SigninForm from '../../components/Signin/index'

const styles = () => ({
  root: {
    height: 'inherit',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  card: {
    width: '350px',
    height: '300px'
  }
})

class SigninScreen extends Component {

  constructor (props) {
    super(props)
    this.state = {
      user: {
        email: '',
        password: '',
        error: {}
      }
    }
    this.handleChangeValue = this.handleChangeValue.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChangeValue (e) {
    let inputName = e.target.name
    let inputValue = e.target.value
    this.setState({user: {...this.state.user, [inputName]: inputValue}})
  }

  handleSubmit = (event) => {
    const {user} = this.state
    const {login} = this.props
    event.preventDefault()
    login(user)
  }

  render () {
    const {classes, errors} = this.props

    const {user} = this.state
    return (
      <div className={classes.root}>
        <Card className={classes.card}>
          <SigninForm userData={user} handleSubmit={this.handleSubmit} errors={errors} />
        </Card>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
})

const mapStateToDispatch = dispatch => ({
  login: (user) => dispatch(loginUser(user))
})

export default connect(
  mapStateToProps,
  mapStateToDispatch
)(withStyles(styles)(SigninScreen))
