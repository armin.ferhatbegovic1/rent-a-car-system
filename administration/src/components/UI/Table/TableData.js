import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import connect from 'react-redux/es/connect/connect'

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto'
  },
  table: {
    minWidth: 700
  }
})

class TableData extends React.Component {
  render () {

    const {data, header, classes} = this.props
    let inc = 0
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              {header.map(function (row) {
                return (<TableCell key={row.key}>{row.label}</TableCell>)
              })}
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map(row => {
              return (
                <TableRow key={inc}>
                  {header.map(function (row2) {
                    inc++
                    if (typeof row2.property !== 'undefined') {
                      const data = row[row2.key]
                      return (<TableCell align='right' key={inc}>{data[row2.property]}</TableCell>)
                    } else {
                      return (<TableCell align='right' key={inc}>{row[row2.key]}</TableCell>)
                    }
                  })}
                </TableRow>
              )
            })}
          </TableBody>
        </Table>
      </Paper>
    )
  }
}

export default connect(
  null
)(withStyles(styles)(TableData))

