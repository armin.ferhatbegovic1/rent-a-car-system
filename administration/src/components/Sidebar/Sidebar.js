import React, { Component } from 'react'
import classNames from 'classnames'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import DashboardIcon from '@material-ui/icons/Dashboard'
import { connect } from 'react-redux'
import { Link, withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import { localize } from '../../services/'
import styles from './styles'
import { Role } from '../../config/Constants'

const menuItems = [

  {
    name: localize('home'),
    path: '/home',
    role: [Role.ADMIN, Role.BRAND]
  },
  {
    name: localize('cars'),
    path: '/cars',
    role: [Role.ADMIN, Role.BRAND]
  },
  {
    name: localize('users'),
    path: '/users',
    role: [Role.ADMIN, Role.BRAND]
  }, {
    name: localize('reservations'),
    path: '/reservations',
    role: [Role.ADMIN, Role.BRAND]
  }
]

class Sidebar extends Component {

  render () {
    const {open, classes, auth} = this.props
    return (
      <Drawer
        variant='permanent'
        classes={{
          paper: classNames(
            classes.drawerPaper,
            !open && classes.drawerPaperClose
          )
        }}
        open={open}
      >
        <List>
          {
            Object.keys(menuItems)
            .map(key => {
              return (
                menuItems[key].role.indexOf(Role.ADMIN) !== -1 ?
                  <Link to={menuItems[key].path} key={key}>
                    <ListItem button>
                      <ListItemIcon>
                        <DashboardIcon />
                      </ListItemIcon>
                      <ListItemText primary={menuItems[key].name} />
                    </ListItem>
                  </Link> : null
              )
            })
          }
        </List>
      </Drawer>
    )
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth,
    errors: state.errors
  }
}
export default connect(mapStateToProps)(withRouter(withStyles(styles)(Sidebar)))