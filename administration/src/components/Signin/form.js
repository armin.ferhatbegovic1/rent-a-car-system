import React from "react";
import { Field, reduxForm } from "redux-form";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { Button } from "@material-ui/core";
import FormLabel from '@material-ui/core/FormLabel'
const styles = () => ({
  grid: {
    padding: "20px",
    display: "grid",
    gridTemplateRows: "85px 1fr 1fr 1fr",
    height: "inherit"
  },
  buttons: {
    display: "flex",
    justifyContent: "end",
    alignItems: "center"
  }
});

const SigninForm = props => {
  const {classes, userData, onChangeValue, handleSubmit, errors} = props
  let errorLabel = null
  if (typeof errors.error !== 'undefined') {
    errorLabel = <FormLabel error={true}>{errors.error}</FormLabel>
  }

  return (
    <form onSubmit={handleSubmit} className={classes.grid}>
      <div>
        <Typography
          align="left"
          headlineMapping={{ display: "h1" }}
          variant="headline"
        >
          Login
        </Typography>
        <Typography
          align="left"
          headlineMapping={{ display: "h1" }}
          variant="title"
        >
          Sigin
        </Typography>
      </div>
      <Field
        className={classes.inputRoot}
        name='email'
        type='email'
        label='Email'
        value={userData.email}
        onChange={onChangeValue}
        component={TextField}
      />
      <Field
        className={classes.inputRoot}
        name='password'
        type='password'
        label='Password'
        component={TextField}
      />
      <div className={classes.buttons}>
        <Button
          type='submit'
          color='primary'
          variant='contained'
        >
          Signin
        </Button>
      </div>
    </form>
  );
};

export default  reduxForm({
  // a unique name for the form
  form: "login"
})(withStyles(styles)(SigninForm));
