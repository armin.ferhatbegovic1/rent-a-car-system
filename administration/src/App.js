import React, { Component } from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import { MuiThemeProvider } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import store from './store'
import { SetAuthToken } from './services/'
import jwt_decode from 'jwt-decode'
import { logoutUser, setCurrentUser } from './store/actions/authentication'
import RoutesList from './components/Routes/Routes'

class App extends Component {

  componentWillMount () {
    if (localStorage.jwtToken) {
      SetAuthToken(localStorage.jwtToken)
      const decoded = jwt_decode(localStorage.jwtToken)
      store.dispatch(setCurrentUser(decoded))
      const currentTime = Date.now() / 1000
      if (decoded.exp < currentTime) {
        store.dispatch(logoutUser())
        window.location.href = '/home'
      }
    }
  }

  render () {
    const {settings, auth} = this.props
    return (
      <MuiThemeProvider theme={settings.theme}>
        <CssBaseline />
        <div style={{height: '100vh'}}>
          <RoutesList isAuthenticated={auth.isAuthenticated} />
        </div>

      </MuiThemeProvider>
    )
  }
}

App.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
}

const mapStateToProps = state => {
  return {
    settings: state.settings,
    auth: state.auth,
    errors: state.errors
  }
}

export default connect(
  mapStateToProps
)(App)
