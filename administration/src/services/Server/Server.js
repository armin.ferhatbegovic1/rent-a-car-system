import request from '../Request/RequestManager'
import { RequestType } from '../../Constants'

const serverHelper = {
  newCar: 'cars/new'
}

export async function insertNewCar (newCarData) {
  const response = await request(RequestType.post, serverHelper.newCar, { newCarData })
  return response
}

