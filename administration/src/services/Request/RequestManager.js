import config from '../../config'

function handleErrors (response) {
  if (!response.ok) {
    throw Error(response.statusText)
  }
  return response
}

async function requestFunction (type, route, body) {
  const {apiEndpoint} = config
  const response = await fetch(apiEndpoint + route, {
    method: type,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: body && JSON.stringify(body)
  }).then(handleErrors).then(function (response) {
    return response.json()
  }).catch(function (error) {
    console.log('fetch error: ', error)
    return ({success: false})
  })
  return response
}

export default requestFunction