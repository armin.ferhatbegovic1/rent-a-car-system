import en from "./en";

const langs = {
  en
};

export default function (lang = "en") {
  return langs[lang];
};