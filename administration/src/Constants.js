export const RequestType = {
  get: 'GET',
  post: 'POST'
}
export const Role={
  ADMIN:'Admin',
  BRAND:'Brand',
  INFLUENCER:'Influencer'
}
