const carsController = require('../controllers').cars
const usersController = require('../controllers').users
const reservationController = require('../controllers').reservations

module.exports = (app) => {
    app.get('/api', (req, res) => res.status(200).send({
        message: 'Welcome to the Todos API!'
    }))

    app.post('/api/cars/new', carsController.create)
    app.get('/api/cars', carsController.list)

    app.post('/api/users/new', usersController.create)
    app.get('/api/users', usersController.list)

    app.post('/api/user/logilistReservationsDetailsn', usersController.loginUser)
    app.post('/api/user/login-customer', usersController.loginCustomer)


    app.post('/api/user/register', usersController.registerUser)
    app.get('/api/user/profile/:user_id', usersController.getProfile)

    app.get('/api/user/profile-by-email/:email', usersController.getProfileByEmail)

    app.post('/api/user/change-password', usersController.changePassword)

    app.get('/api/reservation/get-free-cars/:start_date/:end_date', reservationController.getFreeCarsAtSpecificDate)
    app.get('/api/reservation/list', reservationController.list)

    app.get('/api/reservation/list-reservations/:filter', reservationController.listReservationsDetails)
    app.post('/api/reservation/make-reservation/:start_date/:end_date/:start_time/:end_time/:car_id/:user_id', reservationController.makeReservations)

    app.get('/api/reservation/list-reservations-by-user/:user_id', reservationController.listReservationsDetailsByUser)
}