/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('locations', {
    location_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    /*location: {
      type: DataTypes.ENUM(),
      allowNull: true
    },*/
    campaign_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    type_location: {
      type: DataTypes.STRING,
      allowNull: true
    },
    time: {
      type: DataTypes.TIME,
      allowNull: true
    }
  }, {
    tableName: 'locations'
  });
};
