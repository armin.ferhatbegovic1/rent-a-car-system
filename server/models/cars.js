/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cars', {
    car_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    model: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'cars'
  });
};
