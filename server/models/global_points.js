/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('global_points', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    }
    /*,
    location: {
      type: DataTypes.ENUM(),
      allowNull: true
    }*/
  }, {
    tableName: 'global_points'
  });
};
