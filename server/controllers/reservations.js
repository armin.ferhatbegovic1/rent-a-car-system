const Reservation = require('../models').reservations
const Car = require('../models').cars
const User = require('../models').users
const bcrypt = require('bcryptjs')
const auth = require('basic-auth')
const jwt = require('jsonwebtoken')
const config = require('../config/config.json')
const moment = require('moment')
const db = require('../models/index')
const services = require('./services')

module.exports = {

    makeReservations(req, res) {
        const start_date = req.params.start_date
        const end_date = req.params.end_date
        const start_time = req.params.start_time
        const end_time = req.params.end_time
        const user_id = req.params.user_id
        const car_id = req.params.car_id

        Reservation
            .create({
                reservation_id: '',
                start_date: start_date,
                end_date: end_date,
                start_time: start_time,
                end_time: end_time,
                car_id: car_id,
                user_id: user_id
            })
            .then(data => {
                const token = services.generateAccessToken(data.reservation_id, start_date, end_date, start_time, end_time, car_id, user_id)

                if (token !== '') {
                    console.log("TOKEN===", token)
                    res.status(201).send(token)
                } else {
                    res.status(401).send('Access Token is not generated')
                }
            })
            .catch(error => res.status(400).send(error))
    },
    getFreeCarsAtSpecificDate(req, res) {
        const start_date = req.params.start_date
        const end_date = req.params.end_date
        const start_time = req.params.start_time
        const end_time = req.params.end_time

        db.sequelize.query('SELECT car_id, name, model FROM "cars" AS "car1" WHERE "car1"."car_id" NOT IN ( SELECT "car"."car_id" FROM "reservations" AS "reservations" INNER JOIN "cars" AS "car" ON "reservations"."car_id" = "car"."car_id" WHERE ("reservations"."start_date" BETWEEN $1 AND $2 OR "reservations"."end_date" BETWEEN $1 AND $2));',
            // db.sequelize.query('SELECT*FROM cars',
            {
                bind: [start_date, end_date],
                type: db.sequelize.QueryTypes.SELECT
            }).then(carsList => {
            console.log(carsList)
            res.status(201).send(carsList)
        })
            .catch(error => res.status(400).send(error))
    },
    list(req, res) {
        return Reservation
            .all()
            .then(Reservation => res.status(200).send(Reservation))
            .catch(error => res.status(400).send(error))
    }
    ,
    listReservationsDetails(req, res) {
        const token = req.headers['authorization']
        let decoded = null
        const filter = req.params.filter
        /*  try {
            decoded = jwt.verify(token, config.secret)
          } catch (err) {
            res.status(401).send('Invalid token! ')
          }
      */
        Reservation.belongsTo(Car, {foreignKey: 'car_id'})
        Reservation.belongsTo(User, {foreignKey: 'user_id'})
        let filterParameters = {}
        if (filter === 'activeReservations') {
            filterParameters['end_date'] = {
                $gt: new Date()
            }
        } else {
            filterParameters = {}
        }

        return Reservation.findAll({
            include: [{
                model: Car, required: true
            }, {
                model: User, required: true
            }]
            ,
            where: filterParameters
        })
            .then(Reservation => res.status(200).send(Reservation))
            .catch(error => {
                console.log(error)
                res.status(400).send(error)
            })
    },
    listReservationsDetailsByUser(req, res) {
        const user_id = req.params.user_id

        Reservation.belongsTo(Car, {foreignKey: 'car_id'})
        //  Reservation.belongsTo(User, {foreignKey: 'user_id'})

        return Reservation.findAll({
            include: [{
                model: Car, required: true
            }], where: {user_id: user_id}
        })
            .then(Reservation => {
                const reservationWithToken=[];
                for (let i = 0; i <Reservation.length; i++) {
                    const reservation = Reservation[i];
                    const token = services.generateAccessToken(reservation.reservation_id, reservation.start_date, reservation.end_date, reservation.start_time, reservation.end_time, reservation.car_id, reservation.user_id)

                    reservation['dataValues'].generatedToken = token;
                    reservationWithToken.push(reservation)
                }

                if (reservationWithToken.length>0) {
                    res.status(201).send(reservationWithToken)
                } else {
                    res.status(401).send('Access Token is not generated')
                }
            })


            .catch(error => {
                console.log(error)
                res.status(400).send(error)
            })
    }
}

