const moment = require('moment')

module.exports = {
  generateAccessToken (reservation_id,start_date, end_date, start_time, end_time, car_id, user_id) {

    const reservationID = reservation_id.toString()
    const days = numberOfDays(start_date, end_date).toString()
    const newDays = addDigits(days)

    const time = timeDiff(start_time, end_time).toString()
    const newTime = addDigits(time)

    const carID = car_id.toString()
    const newcarID = addDigits(carID)

    const userID = user_id.toString()
    const newUserID = addDigits(userID)

    const token = reservationID+newDays + newTime + newcarID + newUserID
    return token
  }
}

function addDigits (identifier) {
  const missedDigits = 4 - identifier.length
  if (missedDigits > 0) {
    for (let i = 0; i < missedDigits; i++) {
      identifier += '0'
    }
  }
  return identifier
}

function numberOfDays (start_date, end_date) {
  const startDate = new Date(start_date)
  const endDate = new Date(end_date)
  const oneDay = 24 * 60 * 60 * 1000
  return Math.round(Math.abs((endDate.getTime() - startDate.getTime()) / (oneDay)))

}

function timeDiff (start_time, end_time) {
  const startTime = moment(start_time, 'hh:mm a')
  const endTime = moment(end_time, 'hh:mm a')
  const duration = moment.duration(endTime.diff(startTime))
  const hours = parseInt(duration.asHours())
  const minutes = parseInt(duration.asMinutes()) % 60
  //console.log(hours + ' hour and ' + minutes + ' minutes.')
  return hours + minutes
}


