const cars = require('./cars');
const users = require('./users');
const reservations = require('./reservations');

module.exports = {
  cars,
  users,
  reservations
};