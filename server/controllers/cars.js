const Car = require('../models').cars

module.exports = {
  create (req, res) {
    return Car
    .create({
      car_id: req.body.newCarData.car_id,
      name: req.body.newCarData.name,
      model: req.body.newCarData.model
    })
    .then(todo => res.status(201).send(todo))
    .catch(error => res.status(400).send(error))
  },
  list (req, res) {
    return Car
    .all()
    .then(Car => res.status(200).send(Car))
    .catch(error => res.status(400).send(error))
  }
}