const User = require('../models').users
const bcrypt = require('bcryptjs')
const auth = require('basic-auth')
const jwt = require('jsonwebtoken')
const config = require('../config/config.json')

module.exports = {
  create (req, res) {
    return User
    .create({
      user_id: req.body.newUserData.user_id,
      full_name: req.body.newUserData.full_name,
      email: req.body.newUserData.email,
      password: req.body.newUserData.password,
      created_at: new Date()
    })
    .then(todo => res.status(201).send(todo))
    .catch(error => res.status(400).send(error))
  },
  list (req, res) {
    return User
    .all()
    .then(User => res.status(200).send(User))
    .catch(error => res.status(400).send(error))
  },
  async registerUser (req, res) {

    const email = req.body.email
    const password = req.body.password
    const fullName = req.body.fullName
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync(password, salt)

    if (!fullName || !email || !password || !fullName.trim() || !email.trim() || !password.trim()) {

      res.status(400).json({message: 'Invalid Request !'})

    } else {

      const newUser = {
        full_name: fullName,
        email: email,
        password: hash
        //created_at: new Date()
      }

      await new Promise((resolve, reject) => {
        User.findOne({where: {email: newUser.email}})
        .then(users => {
          if (users === null) {
            User.create(newUser)
            .then(() => res.status(201).send('User Registered Sucessfully !'))
            .catch(err => {
              res.status(500).send('Internal Server Error !')
            })
          } else {
            res.status(409).send('User Already Registered !')
          }
        })
        .catch(res.status({status: 500, message: 'Internal Server Error !'}))
      })
    }
  },
  async loginCustomer (req, res) {

    console.log('credentials')
    const credentials = auth(req)

    const email = credentials.name
    const password = credentials.pass

    if (!email || !password || !email.trim() || !password.trim()) {
      res.status(400).json({message: 'Invalid Request (Email or Password is empty)!'})

    } else {

      await new Promise((resolve, reject) => {
        User.findOne({where: {email: email}})
        .then(foundedUser => {
          if (foundedUser !== null) {

            const hashed_password = foundedUser.password
            if (bcrypt.compareSync(password, hashed_password)) {

              const token = jwt.sign(foundedUser.toJSON(), config.secret, {expiresIn: 1440})

              res.status(200).json({message: foundedUser['email'], token: token, foundedUser})
            } else {
              res.status(401).send('Invalide Credentials!')
            }
          } else {
            res.status(401).send('Invalide Credentials')

          }
        })
        .catch(res.status({status: 500, message: 'Internal Server Error !'}))
      })
    }
  },
  async loginUser (req, res) {
    const email = req.body.email
    const password = req.body.password

    if (!email || !password || !email.trim() || !password.trim()) {

      res.status(400).json({message: 'Invalid Request !'})

    } else {
      const user = {
        email: email,
        password: password
        //created_at: new Date()
      }
      await new Promise((resolve, reject) => {
        User.findOne({where: {email: user.email}})
        .then(foundedUser => {
          if (foundedUser !== null) {

            const hashed_password = foundedUser.password
            if (bcrypt.compareSync(user.password, hashed_password)) {

              const token = jwt.sign(foundedUser.toJSON(), config.secret, {expiresIn: 1440})

              res.status(200).json({message: 'Found User', token: token, foundedUser})
            } else {
              res.status(401).send('Invalide Credentials!')
            }
          } else {
            res.status(401).send('Invalide Credentials')

          }
        })
        .catch(res.status({status: 500, message: 'Internal Server Error !'}))
      })
    }
  },

  async getProfile (req, res) {
    const user_id = req.params.user_id
    const token = req.headers['Authorization']
    let decoded = null
    try {
      decoded = jwt.verify(token, config.secret)
    } catch (err) {
      res.status(401).send('Invalid token!')
    }

    await new Promise((resolve, reject) => {
      if (decoded.user_id == user_id) {
        User.findOne({
          where: {user_id: user_id},
          attributes: ['user_id', 'full_name', 'email', 'driver_license', 'dot', 'country', 'role']
        })
        .then(users => {
          if (users !== null) {
            res.status(200).send(users)
          } else {
            res.status(400).send('User Not Exist !')
          }
        })
        .catch(res.status({status: 500, message: 'Internal Server Error !'}))
      } else {
        res.status(401).send('Invalid token!')
      }
    })

  },
  async getProfileByEmail (req, res) {

    const email = req.params.email

    const token = req.headers['x-access-token']
    let decoded = null
    try {
      decoded = jwt.verify(token, config.secret)
    } catch (err) {

      res.status(401).send('Invalid token!')
    }
    await new Promise((resolve, reject) => {
      if (decoded.email == email) {
        User.findOne({
          where: {email: decoded.email },
          attributes: ['user_id', 'full_name', 'email', 'driver_license', 'dot', 'country', 'role','password']
        })
        .then(users => {
          if (users !== null) {
            res.status(200).send(users)
          } else {
            res.status(400).send('User Not Exist !')
          }
        })
        .catch(res.status({status: 500, message: 'Internal Server Error !'}))
      } else {
        res.status(401).send('Invalid token!')
      }
    })

  },
  async changePassword (req, res) {
    const email = req.body.email
    const password = req.body.password
    const newPassword = req.body.newPassword

    await new Promise((resolve, reject) => {
      User.findOne({where: {email: email}})
      .then(foundedUser => {
        if (foundedUser !== null) {
          console.log(foundedUser)
          const hashed_password = foundedUser.password

          if (bcrypt.compareSync(password, hashed_password)) {

            const salt = bcrypt.genSaltSync(10)
            const hash = bcrypt.hashSync(newPassword, salt)
            foundedUser.update({password: hash})
            res.status(201).send('User Updated Sucessfully !')
          } else {
            res.status(401).send('Invalid Old Password !')
          }
        } else {
          res.status(400).send('User Not Exist !')
        }
      }).catch(res.status({status: 500, message: 'Internal Server Error !'}))

    })
  }

}