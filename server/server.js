const express = require('express')
const bodyParser = require('body-parser')
const app = express()

app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers,Authorization, Origin,Accept,x-access-token X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);


  // Pass to next layer of middleware
  next();
});
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
require('./routes/routes')(app)


app.get('*', (req, res) => res.status(200).send({
  message: 'Welcome to the beginning of nothingness.'
}))

app.get('/', (req, res, next) => {
  res.send('Civil management')
})

app.listen(4000, () => {
  console.log('Listing on port 4000')
})
